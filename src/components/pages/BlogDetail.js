import React, { Component, Fragment } from 'react'
import Helmet from 'react-helmet'
import { Container, Row, Col } from 'react-bootstrap'
import { Spinner } from 'react-bootstrap'

import DetailBlog from '../content/blog/detailblog'

import Search from '../content/blog/Search'
import Category from '../content/blog/Category'
import RecentArticle from '../content/blog/RecentArticle'
import WhatsApp from '../content/stiky/WhatsApp'

class BlogDetail extends Component {
    state = {
        dtlBlog: []
    }

    componentDidMount() {
        this.loadDetailBlog()
    }

    loadDetailBlog = (e) => {
        const { dtlBlog } = this.state;
        const titleSlug = this.props.match.params.slug;
        const url = process.env.REACT_APP_API_URL + `/newsapi/` + titleSlug
        fetch(url, { headers: { Authorization: 'Bearer '.concat(process.env.REACT_APP_TOKEN) } })
        .then(res=>res.json())
        .then(json => this.setState({
            dtlBlog : [...dtlBlog, ...json.data],
        }))
    }
    render() {
        return (
            <Fragment>
                {
                    this.state.dtlBlog.map((detailBlog ,i) => {
                        return(
                        <Helmet key={i}>
                            <meta charset="utf-8" />
                            <meta name="viewport" content="width=device-width, initial-scale=1" />
                            <meta name="theme-color" content="#000000" />
                            <link rel="icon" href="/favicon-apple.png" sizes="192x192" />
                            <link rel="apple-touch-icon" href="/favicon-apple.png" />
                            <link rel="canonical" href={"https://firesystem.co.id/blog/" + this.props.match.params.categori + "/" + this.props.match.params.slug}/>
                            <link rel='shortlink' href={"https://firesystem.co.id/blog/" + this.props.match.params.categori + "/" + this.props.match.params.slug}/>
                            {/* Primary Meta Tags */}
                            <title>{detailBlog.title}</title>
                            <meta name="title" content={detailBlog.title}/>
                            <meta name="description" content={detailBlog.content.slice(0, 136).replace(/(<([^>]+)>)/ig, '')}/>
                            <meta name="keywords" content={detailBlog.tags}/>

                            <meta itemprop="title" content={detailBlog.title}/>
                            <meta itemprop="description" content={detailBlog.content.slice(0, 136).replace(/(<([^>]+)>)/ig, '')}/>
                            <meta itemprop="image" content={"https://dashboard.firesystem.co.id/storage/thumbnail/" + detailBlog.thumbnail}/>

                            {/* Open Graph / Facebook */}
                            <meta property="og:type" content="website"/>
                            <meta property="og:url" content="https://firesystem.co.id/blog"/>
                            <meta property="og:title" content={detailBlog.title}/>
                            <meta property="og:description" content={detailBlog.content.slice(0, 136).replace(/(<([^>]+)>)/ig, '')}/>
                            <meta property="og:image" content={"https://dashboard.firesystem.co.id/storage/thumbnail/" + detailBlog.thumbnail}/>

                            {/* Twitter */}
                            <meta property="twitter:card" content="summary_large_image"/>
                            <meta property="twitter:url" content="https://firesystem.co.id/blog"/>
                            <meta property="twitter:title" content={detailBlog.title}/>
                            <meta property="twitter:description" content={detailBlog.content.slice(0, 136).replace(/(<([^>]+)>)/ig, '')}/>
                            <meta property="twitter:image" content={"https://dashboard.firesystem.co.id/storage/thumbnail/" + detailBlog.thumbnail}/>
                            
                            <meta name="robots" content="index, follow"/>
                            <meta name="googlebot" content="index, follow"/>
                            <meta name="googlebot-news" content="index, follow"/>
                        </Helmet>
                        );
                    })
                }
                <WhatsApp/>
                <section className="hero-area" style={{backgroundImage: "url(/img/banner/Blog_detail_blog_fire_system-min.jpg)"}}>
                    <Container>
                        <div className="section-heading">
                        {
                            this.state.dtlBlog.map((detailBlog, i) => {
                                return(
                                    <h1 className="title" key={i}>{detailBlog.title}</h1>
                                );
                            })
                        }
                        </div>
                    </Container>
                </section>
                <Container>
                    <Row>
                        <Col xs={12} lg={8}>
                        {
                            this.state.dtlBlog.length > 0 ? this.state.dtlBlog.map((detailBlog, i) => {
                                return(
                                    <DetailBlog  {...detailBlog} key={i}/>
                                );
                            }) : 
                            <div className="d-flex align-items-center justify-content-center text-center py-5">
                                <Spinner animation="border" variant="danger" role="status"><span className="sr-only">Loading...</span></Spinner>
                            </div>
                        }
                        </Col>
                        <Col xs={12} lg={4}>
                            <Search/>
                            <Category/>
                            <RecentArticle/>
                        </Col>
                    </Row>
                </Container>

            </Fragment>
        )
    }
}

export default BlogDetail;