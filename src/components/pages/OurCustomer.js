import React, { Component, Fragment } from 'react'
import Helmet from 'react-helmet'

import Hero from '../content/customer/Hero'
import Content from '../content/customer/Content'
import Gallery from '../content/customer/Gallery'
import NewsLetter from '../content/customer/Newsletter'
import WOW from 'wowjs'
import WhatsApp from '../content/stiky/WhatsApp'

class OurCustomer extends Component {
    componentDidMount() {
        new WOW.WOW({
            live: false
        }).init();
    }
    render() {
        return (
            <Fragment>
                <Helmet>
                    <meta charset="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <meta name="theme-color" content="#000000" />
                    <link rel="icon" href="/favicon-apple.png" sizes="192x192" />
                    <link rel="apple-touch-icon" href="/favicon-apple.png" />
                    <link rel="canonical" href="https://firesystem.co.id/ourcustomers" />
                    <link rel='shortlink' href='https://firesystem.co.id/ourcustomers' />
                    {/* Primary Meta Tags */}
                    <title>Fire System, Fire Protection terbaik & berpengalaman, Fire Alarm System, instalasi alat pemadam kebakaran</title>
                    <meta name="title" content="Fire System, Fire Protection terbaik & berpengalaman, Fire Alarm System, instalasi alat pemadam kebakaran"/>
                    <meta name="description" content="Fire system Produk PT. RASA NDAY SUKSINDO telah digunakan oleh berbagai macam lembaga di Indonesia, baik korporasi, lembaga kesehatan & kementerian. fire system suksindo"/>
                    <meta name="keywords" content="jual produk alat pemadam kebakaran, jasa instalasi fire system, tabung pemadam api, alat kebakaran, fire system, alarm system, fire protection terbaik, hydrant system, fire system berpengalaman"/>

                    <meta itemprop="title" content="Fire System, Fire Protection terbaik & berpengalaman, Fire Alarm System, instalasi alat pemadam kebakaran"/>
                    <meta itemprop="description" content="Fire system Produk PT. RASA NDAY SUKSINDO telah digunakan oleh berbagai macam lembaga di Indonesia, baik korporasi, lembaga kesehatan & kementerian. fire system suksindo"/>
                    <meta itemprop="image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Open Graph / Facebook */}
                    <meta property="og:type" content="website"/>
                    <meta property="og:url" content="https://firesystem.co.id/ourcustomers"/>
                    <meta property="og:title" content="Fire System, Fire Protection terbaik & berpengalaman, Fire Alarm System, instalasi alat pemadam kebakaran"/>
                    <meta property="og:description" content="Fire system Produk PT. RASA NDAY SUKSINDO telah digunakan oleh berbagai macam lembaga di Indonesia, baik korporasi, lembaga kesehatan & kementerian. fire system suksindo"/>
                    <meta property="og:image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Twitter */}
                    <meta property="twitter:card" content="summary_large_image"/>
                    <meta property="twitter:url" content="https://firesystem.co.id/ourcustomers"/>
                    <meta property="twitter:title" content="Fire System, Fire Protection terbaik & berpengalaman, Fire Alarm System, instalasi alat pemadam kebakaran"/>
                    <meta property="twitter:description" content="Fire system Produk PT. RASA NDAY SUKSINDO telah digunakan oleh berbagai macam lembaga di Indonesia, baik korporasi, lembaga kesehatan & kementerian. fire system suksindo"/>
                    <meta property="twitter:image" content="https://firesystem.co.id/logo/logo.png"/>
                    
                    <meta name="robots" content="index, follow"/>
                    <meta name="googlebot" content="index, follow"/>
                    <meta name="googlebot-news" content="index, follow"/>
                </Helmet>
                <WhatsApp/>
                <Hero/>
                <Content/>
                <Gallery/>
                <NewsLetter/>
            </Fragment>
        );
    }
}

export default OurCustomer;    
