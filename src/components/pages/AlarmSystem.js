import React, { Component, Fragment } from 'react'
import Helmet from 'react-helmet'

import Hero from '../content/alarmsystem/Hero'
import Content from '../content/alarmsystem/Content'
import Gallery from '../content/alarmsystem/GalleryProduct'
import NewsLetter from '../content/alarmsystem/Newsletter'
import WOW from 'wowjs'
import WhatsApp from '../content/stiky/WhatsApp'

class AlarmSystem extends Component {
    componentDidMount() {
        new WOW.WOW({
            live: false
        }).init();
    }
    render() {
        return (
            <Fragment>
                <Helmet>
                    <meta charset="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <meta name="theme-color" content="#000000" />
                    <link rel="icon" href="/favicon-apple.png" sizes="192x192" />
                    <link rel="apple-touch-icon" href="/favicon-apple.png" />
                    <link rel="canonical" href="https://firesystem.co.id/alarm-system" />
                    <link rel='shortlink' href='https://firesystem.co.id/alarm-system' />
                    {/* Primary Meta Tags */}
                    <title>Fire Supression System, Fire Alarm System, Fire Protection</title>
                    <meta name="title" content="Fire Supression System, Fire Alarm System, Fire Protection"/>
                    <meta name="description" content="Fire Supression System merupakan salah satu solusi untuk menjaga data atau alat berharga anda dari potensi kebakaran. Secara otomatis, Fire Suppression System. firesystem."/>
                    <meta name="keywords" content="Fire system, fire supression system, fire protection, pt rasa nday suksindo, distributor alat pemadam kebakaran, fire protection terbaik, instalasi hydrant, fire supression system media co2, fire protection terbaik di jakarta"/>

                    <meta itemprop="title" content="Fire Supression System, Fire Alarm System, Fire Protection"/>
                    <meta itemprop="description" content="Fire Supression System merupakan salah satu solusi untuk menjaga data atau alat berharga anda dari potensi kebakaran. Secara otomatis, Fire Suppression System. firesystem."/>
                    <meta itemprop="image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Open Graph / Facebook */}
                    <meta property="og:type" content="website"/>
                    <meta property="og:url" content="https://firesystem.co.id/alarm-system"/>
                    <meta property="og:title" content="Fire Supression System, Fire Alarm System, Fire Protection"/>
                    <meta property="og:description" content="Fire Supression System merupakan salah satu solusi untuk menjaga data atau alat berharga anda dari potensi kebakaran. Secara otomatis, Fire Suppression System. firesystem."/>
                    <meta property="og:image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Twitter */}
                    <meta property="twitter:card" content="summary_large_image"/>
                    <meta property="twitter:url" content="https://firesystem.co.id/alarm-system"/>
                    <meta property="twitter:title" content="Fire Supression System, Fire Alarm System, Fire Protection"/>
                    <meta property="twitter:description" content="Fire Supression System merupakan salah satu solusi untuk menjaga data atau alat berharga anda dari potensi kebakaran. Secara otomatis, Fire Suppression System. firesystem."/>
                    <meta property="twitter:image" content="https://firesystem.co.id/logo/logo.png"/>
                    
                    <meta name="robots" content="index, follow"/>
                    <meta name="googlebot" content="index, follow"/>
                    <meta name="googlebot-news" content="index, follow"/>
                </Helmet>
                <WhatsApp/>
                <Hero/>
                <Content/>
                <Gallery/>
                <NewsLetter/>
            </Fragment>
        );
    }
}

export default AlarmSystem;    