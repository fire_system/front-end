import React, { Component, Fragment } from 'react'
import Helmet from 'react-helmet'

import Hero from '../content/hydrantsystem/Hero'
import Content from '../content/hydrantsystem/Content'
import Gallery from '../content/hydrantsystem/Gallery'
import NewsLetter from '../content/hydrantsystem/Newsletter'
import WOW from 'wowjs'
import WhatsApp from '../content/stiky/WhatsApp'


class HydrantSystem extends Component {
    componentDidMount() {
        new WOW.WOW({
            live: false
        }).init();
    }
    render() {
        return (
            <Fragment>
                <Helmet>
                    <meta charset="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <meta name="theme-color" content="#000000" />
                    <link rel="icon" href="/favicon-apple.png" sizes="192x192" />
                    <link rel="apple-touch-icon" href="/favicon-apple.png" />
                    <link rel="canonical" href="https://firesystem.co.id/hydrant-system" />
                    <link rel='shortlink' href='https://firesystem.co.id/hydrant-system' />
                    {/* Primary Meta Tags */}
                    <title>Fire Hydrant, Fire System, System Fire Hydrant</title>
                    <meta name="title" content="Fire Hydrant, Fire System, System Fire Hydrant"/>
                    <meta name="description" content="Fire hydrant suatu sistem instalasi berisi air bertekanan besar yang digunakan sebagai sarana memadamkan kebakaran. percayakan pada kami untuk instalasi fire hydrant."/>
                    <meta name="keywords" content="Fire system, fire hydrant, fire protection, pompa hydrant, kontraktor instalasi fire system, instalasi, fire hydrant"/>

                    <meta itemprop="title" content="Fire Hydrant, Fire System, System Fire Hydrant"/>
                    <meta itemprop="description" content="Fire hydrant suatu sistem instalasi berisi air bertekanan besar yang digunakan sebagai sarana memadamkan kebakaran. percayakan pada kami untuk instalasi fire hydrant."/>
                    <meta itemprop="image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Open Graph / Facebook */}
                    <meta property="og:type" content="website"/>
                    <meta property="og:url" content="https://firesystem.co.id/hydrant-system"/>
                    <meta property="og:title" content="Fire Hydrant, Fire System, System Fire Hydrant"/>
                    <meta property="og:description" content="Fire hydrant suatu sistem instalasi berisi air bertekanan besar yang digunakan sebagai sarana memadamkan kebakaran. percayakan pada kami untuk instalasi fire hydrant."/>
                    <meta property="og:image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Twitter */}
                    <meta property="twitter:card" content="summary_large_image"/>
                    <meta property="twitter:url" content="https://firesystem.co.id/hydrant-system"/>
                    <meta property="twitter:title" content="Fire Hydrant, Fire System, System Fire Hydrant"/>
                    <meta property="twitter:description" content="Fire hydrant suatu sistem instalasi berisi air bertekanan besar yang digunakan sebagai sarana memadamkan kebakaran. percayakan pada kami untuk instalasi fire hydrant."/>
                    <meta property="twitter:image" content="https://firesystem.co.id/logo/logo.png"/>
                    
                    <meta name="robots" content="index, follow"/>
                    <meta name="googlebot" content="index, follow"/>
                    <meta name="googlebot-news" content="index, follow"/>
                </Helmet>
                <WhatsApp/>
                <Hero/>
                <Content/>
                <Gallery/>
                <NewsLetter/>
            </Fragment>
        )
    }
}

export default HydrantSystem;