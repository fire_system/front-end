import React, { Component, Fragment } from 'react'
import Hero from '../content/contact/Hero'
import Form from '../content/contact/ContactFrom'
import Maps from '../content/contact/Maps'
import Newsletter from '../content/contact/Newsletter'

import Helmet from 'react-helmet'
import WOW from 'wowjs'
import WhatsApp from '../content/stiky/WhatsApp'

class Contact extends Component {
    componentDidMount() {
        new WOW.WOW({
            live: false
        }).init();
    }
    render() {
        return(
            <Fragment>
                <Helmet>
                    <meta charset="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <meta name="theme-color" content="#000000" />
                    <link rel="icon" href="/favicon-apple.png" sizes="192x192" />
                    <link rel="apple-touch-icon" href="/favicon-apple.png" />
                    <link rel="canonical" href="https://firesystem.co.id/contact" />
                    <link rel='shortlink' href='https://firesystem.co.id/contact' />
                    {/* Primary Meta Tags */}
                    <title>Fire System, Pt Rasa Nday Suksindo, Fire Alarm System, Fire Protection, Hydrant System</title>
                    <meta name="title" content="Fire System, Pt Rasa Nday Suksindo, Fire Alarm System, Fire Protection, Hydrant System"/>
                    <meta name="description" content="Kami menyediakan produk dan jasa alat pemadam kebakaran meliputi Fire System di antaranya penjualan dan Instalasi Produk Fire Alarm, Fire Hydrant, Fire Suppression System."/>
                    <meta name="keywords" content="Fire system, alarm system, alat pemadam kebaran, instalasi fire hydrant, fire safety, fire protection berpengalaman, fire hose, fire suit, fire extinguisher, pt rasa ndya suksindo, fire protection terbaik, fire protection berpengalaman, instalasi alat pemadam kebakaran"/>

                    <meta itemprop="title" content="Fire System, Pt Rasa Nday Suksindo, Fire Alarm System, Fire Protection, Hydrant System"/>
                    <meta itemprop="description" content="Kami menyediakan produk dan jasa alat pemadam kebakaran meliputi Fire System di antaranya penjualan dan Instalasi Produk Fire Alarm, Fire Hydrant, Fire Suppression System."/>
                    <meta itemprop="image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Open Graph / Facebook */}
                    <meta property="og:type" content="website"/>
                    <meta property="og:url" content="https://firesystem.co.id/contact"/>
                    <meta property="og:title" content="Fire System, Pt Rasa Nday Suksindo, Fire Alarm System, Fire Protection, Hydrant System"/>
                    <meta property="og:description" content="Kami menyediakan produk dan jasa alat pemadam kebakaran meliputi Fire System di antaranya penjualan dan Instalasi Produk Fire Alarm, Fire Hydrant, Fire Suppression System."/>
                    <meta property="og:image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Twitter */}
                    <meta property="twitter:card" content="summary_large_image"/>
                    <meta property="twitter:url" content="https://firesystem.co.id/contact"/>
                    <meta property="twitter:title" content="Fire System, Pt Rasa Nday Suksindo, Fire Alarm System, Fire Protection, Hydrant System"/>
                    <meta property="twitter:description" content="Kami menyediakan produk dan jasa alat pemadam kebakaran meliputi Fire System di antaranya penjualan dan Instalasi Produk Fire Alarm, Fire Hydrant, Fire Suppression System."/>
                    <meta property="twitter:image" content="https://firesystem.co.id/logo/logo.png"/>
                    
                    <meta name="robots" content="index, follow"/>
                    <meta name="googlebot" content="index, follow"/>
                    <meta name="googlebot-news" content="index, follow"/>
                </Helmet>
                <WhatsApp/>
                <Hero/>
                <Form/>
                <Maps/>
                <Newsletter/>
            </Fragment>
        );
    }
}

export default Contact;