import React, { Component, Fragment } from 'react'
import Helmet from 'react-helmet'

import Hero from '../content/fireextinguisher/Hero'
import Content from '../content/fireextinguisher/Content'
import Serve from '../content/fireextinguisher/Serve'
import Gallery from '../content/fireextinguisher/GalleryProduct'
import NewsLetter from '../content/fireextinguisher/Newsletter'
import WOW from 'wowjs'
import WhatsApp from '../content/stiky/WhatsApp'

class FireExtinguisherAparApab extends Component {
    componentDidMount() {
        new WOW.WOW({
            live: false
        }).init();
    }
    render() {
        return (
            <Fragment>
                <Helmet>
                    <meta charset="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <meta name="theme-color" content="#000000" />
                    <link rel="icon" href="/favicon-apple.png" sizes="192x192" />
                    <link rel="apple-touch-icon" href="/favicon-apple.png" />
                    <link rel="canonical" href="https://firesystem.co.id/fire-extinguisher-apar-apab" />
                    <link rel='shortlink' href='https://firesystem.co.id/fire-extinguisher-apar-apab' />
                    {/* Primary Meta Tags */}
                    <title>Fire Extinguisher, Fire System, alat pemadam api, distributor alat pemadam kebakaran</title>
                    <meta name="title" content="Fire Extinguisher, Fire System, alat pemadam api, distributor alat pemadam kebakaran"/>
                    <meta name="description" content="PT. RASA NDAY SUKSINDO melayani segala kebutuhan alat pemadam api. kami menyediakan produk alat pemadam api skala kecil maupun besar dan juga pengisian ulang."/>
                    <meta name="keywords" content="Fire extinguisher, fire system, tabung pemadam kebakaran, foam fire extinguisher, instalasi alat pemadam kebakaran"/>

                    <meta itemprop="title" content="Fire Extinguisher, Fire System, alat pemadam api, distributor alat pemadam kebakaran"/>
                    <meta itemprop="description" content="PT. RASA NDAY SUKSINDO melayani segala kebutuhan alat pemadam api. kami menyediakan produk alat pemadam api skala kecil maupun besar dan juga pengisian ulang."/>
                    <meta itemprop="image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Open Graph / Facebook */}
                    <meta property="og:type" content="website"/>
                    <meta property="og:url" content="https://firesystem.co.id/fire-extinguisher-apar-apab"/>
                    <meta property="og:title" content="Fire Extinguisher, Fire System, alat pemadam api, distributor alat pemadam kebakaran"/>
                    <meta property="og:description" content="PT. RASA NDAY SUKSINDO melayani segala kebutuhan alat pemadam api. kami menyediakan produk alat pemadam api skala kecil maupun besar dan juga pengisian ulang."/>
                    <meta property="og:image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Twitter */}
                    <meta property="twitter:card" content="summary_large_image"/>
                    <meta property="twitter:url" content="https://firesystem.co.id/fire-extinguisher-apar-apab"/>
                    <meta property="twitter:title" content="Fire Extinguisher, Fire System, alat pemadam api, distributor alat pemadam kebakaran"/>
                    <meta property="twitter:description" content="PT. RASA NDAY SUKSINDO melayani segala kebutuhan alat pemadam api. kami menyediakan produk alat pemadam api skala kecil maupun besar dan juga pengisian ulang."/>
                    <meta property="twitter:image" content="https://firesystem.co.id/logo/logo.png"/>
                    
                    <meta name="robots" content="index, follow"/>
                    <meta name="googlebot" content="index, follow"/>
                    <meta name="googlebot-news" content="index, follow"/>
                </Helmet>
                <WhatsApp/>
                <Hero/>
                <Content/>
                <Serve/>
                <Gallery/>
                <NewsLetter/>
            </Fragment>
        );
    }
}

export default FireExtinguisherAparApab;    