import React, { Component, Fragment } from 'react'
import Helmet from 'react-helmet'

import Hero from '../content/firesupressionsystem/Hero'
import Content from '../content/firesupressionsystem/Content'
import Gallery from '../content/firesupressionsystem/GalleryProduct'
import NewsLetter from '../content/firesupressionsystem/Newsletter'
import WOW from 'wowjs'
import WhatsApp from '../content/stiky/WhatsApp'

class FireSupressionSystem extends Component {
    componentDidMount() {
        new WOW.WOW({
            live: false
        }).init();
    }
    render() {
        return (
            <Fragment>
                <Helmet>
                    <meta charset="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <meta name="theme-color" content="#000000" />
                    <link rel="icon" href="/favicon-apple.png" sizes="192x192" />
                    <link rel="apple-touch-icon" href="/favicon-apple.png" />
                    <link rel="canonical" href="https://firesystem.co.id/fire-supression-system" />
                    <link rel='shortlink' href='https://firesystem.co.id/fire-supression-system' />
                    {/* Primary Meta Tags */}
                    <title>Fire Supression System, Fire System, Fire Alarm System</title>
                    <meta name="title" content="Fire Supression System, Fire System, Fire Alarm System"/>
                    <meta name="description" content="SUKSINDO menyediakan serangkaian lengkap perangkat alarm system seperti Detector (heat and Smoke), Panel System dan Sounder (Bell,Horn). Fire system kami terbaik."/>
                    <meta name="keywords" content="Fire alarm system, fire system, Pt rasa nday suksindo, instalasi pemadam kebakaran, alat pemadam kebakaran, fire supression system, distributor alat pemadam kebakaran di indoesia, kontraktor berpengalaman."/>

                    <meta itemprop="title" content="Fire Supression System, Fire System, Fire Alarm System"/>
                    <meta itemprop="description" content="SUKSINDO menyediakan serangkaian lengkap perangkat alarm system seperti Detector (heat and Smoke), Panel System dan Sounder (Bell,Horn). Fire system kami terbaik."/>
                    <meta itemprop="image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Open Graph / Facebook */}
                    <meta property="og:type" content="website"/>
                    <meta property="og:url" content="https://firesystem.co.id/fire-supression-system"/>
                    <meta property="og:title" content="Fire Supression System, Fire System, Fire Alarm System"/>
                    <meta property="og:description" content="SUKSINDO menyediakan serangkaian lengkap perangkat alarm system seperti Detector (heat and Smoke), Panel System dan Sounder (Bell,Horn). Fire system kami terbaik."/>
                    <meta property="og:image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Twitter */}
                    <meta property="twitter:card" content="summary_large_image"/>
                    <meta property="twitter:url" content="https://firesystem.co.id/fire-supression-system"/>
                    <meta property="twitter:title" content="Fire Supression System, Fire System, Fire Alarm System"/>
                    <meta property="twitter:description" content="SUKSINDO menyediakan serangkaian lengkap perangkat alarm system seperti Detector (heat and Smoke), Panel System dan Sounder (Bell,Horn). Fire system kami terbaik."/>
                    <meta property="twitter:image" content="https://firesystem.co.id/logo/logo.png"/>
                    
                    <meta name="robots" content="index, follow"/>
                    <meta name="googlebot" content="index, follow"/>
                    <meta name="googlebot-news" content="index, follow"/>
                </Helmet>
                <WhatsApp/>
                <Hero/>
                <Content/>
                <Gallery/>
                <NewsLetter/>
            </Fragment>
        );
    }
}

export default FireSupressionSystem;    