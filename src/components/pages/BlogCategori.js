import React, { Component, Fragment } from 'react'
import Helmet from 'react-helmet'
import { Container, Row, Col } from 'react-bootstrap'

import CategoriBlog from '../content/blog/categoriblog'

import Search from '../content/blog/Search'
import Category from '../content/blog/Category'
import RecentArticle from '../content/blog/RecentArticle'
import WhatsApp from '../content/stiky/WhatsApp'

class BlogCategori extends Component {
    state = {
        listCategori: [],
        per: 4,
        page: 1,
        totalPages: null,
        scrolling: false,
    }

    componentDidMount() {
        this.loadListBlog()
        this.scrollListener = window.addEventListener('scroll', (e) => {
            this.handleScrollBlog(e)
        })
    }

    handleScrollBlog = (e) => {
        const { scrolling, totalPages, page } = this.state
        if (scrolling) return
        if (totalPages <= page) return
        const lastLi = document.querySelector('ul.list-blog-array > li:last-child')
        const lastLiOffset = lastLi.offsetTop + lastLi.clientHeight
        const pageOffset = window.pageYOffset + window.innerHeight
        var bottomOffset = 4
        if (pageOffset > lastLiOffset - bottomOffset) this.loadMore()
    }
    
    loadListBlog = () => {
        const { per, page, listCategori } = this.state;
        const slugCategori = this.props.match.params.categori;
        const url = process.env.REACT_APP_API_URL + `/newscategory/`+ slugCategori +`?per=${per}&page=${page}`
        // console.log(url);
        fetch(url, { headers: { Authorization: 'Bearer '.concat(process.env.REACT_APP_TOKEN) } })
        .then(response => response.json())
        .then(json => this.setState({
            listCategori : [...listCategori, ...json.data.sources],
            scrolling: false,
            totalPages: json.data.paginate.total_pages,
        }))
    }

    loadMore = () => {
        this.setState(prevState => ({
            page: prevState.page + 1,
            scrolling: true,
        }), this.loadListBlog)
    }
    render(){
        // console.log(this.state.categoriBlog);
        return (
            <Fragment>
                <Helmet>
                    <meta charset="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <meta name="theme-color" content="#000000" />
                    <link rel="icon" href="/favicon-apple.png" sizes="192x192" />
                    <link rel="apple-touch-icon" href="/favicon-apple.png" />
                    <link rel="canonical" href="https://firesystem.co.id/blog" />
                    <link rel='shortlink' href='https://firesystem.co.id/blog' />
                    {/* Primary Meta Tags */}
                    <title>PT Rasa Nday Suksindo, Fire Protection, Fire Safety, Fire System, Fire Alarm System</title>
                    <meta name="title" content="PT Rasa Nday Suksindo, Fire Protection, Fire Safety, Fire System, Fire Alarm System"/>
                    <meta name="description" content="Produk dan jasa alat pemadam kebakaran meliputi Fire System di antaranya penjualan dan Instalasi Produk Fire Alarm, Fire Hydrant, Fire Suppression System."/>
                    <meta name="keywords" content="jual produk alat pemadam kebakaran, jasa instalasi fire system, tabung pemadam api, alat kebakaran, fire system, alarm system, fire protection terbaik, hydrant system"/>

                    <meta itemprop="title" content="PT Rasa Nday Suksindo, Fire Protection, Fire Safety, Fire System, Fire Alarm System"/>
                    <meta itemprop="description" content="Produk dan jasa alat pemadam kebakaran meliputi Fire System di antaranya penjualan dan Instalasi Produk Fire Alarm, Fire Hydrant, Fire Suppression System."/>
                    <meta itemprop="image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Open Graph / Facebook */}
                    <meta property="og:type" content="website"/>
                    <meta property="og:url" content="https://firesystem.co.id/blog"/>
                    <meta property="og:title" content="PT Rasa Nday Suksindo, Fire Protection, Fire Safety, Fire System, Fire Alarm System"/>
                    <meta property="og:description" content="Produk dan jasa alat pemadam kebakaran meliputi Fire System di antaranya penjualan dan Instalasi Produk Fire Alarm, Fire Hydrant, Fire Suppression System."/>
                    <meta property="og:image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Twitter */}
                    <meta property="twitter:card" content="summary_large_image"/>
                    <meta property="twitter:url" content="https://firesystem.co.id/blog"/>
                    <meta property="twitter:title" content="PT Rasa Nday Suksindo, Fire Protection, Fire Safety, Fire System, Fire Alarm System"/>
                    <meta property="twitter:description" content="Produk dan jasa alat pemadam kebakaran meliputi Fire System di antaranya penjualan dan Instalasi Produk Fire Alarm, Fire Hydrant, Fire Suppression System."/>
                    <meta property="twitter:image" content="https://firesystem.co.id/logo/logo.png"/>
                    
                    <meta name="robots" content="index, follow"/>
                    <meta name="googlebot" content="index, follow"/>
                    <meta name="googlebot-news" content="index, follow"/>
                </Helmet>
                <WhatsApp/>
                <section className="hero-area" style={{backgroundImage: "url(/img/banner/Blog_detail_blog_fire_system-min.jpg)"}}>
                    <Container>
                        <div className="section-heading">
                        {
                            this.state.listCategori.map((arrCategori, i) => {
                                return(
                                    <h1 className="title" key={i}>{arrCategori.category_name}</h1>
                                );
                            })
                        }
                        </div>
                    </Container>
                </section>
                <Container>
                    <Row>
                        <Col xs={12} lg={8}>
                        <section className="contentblog-area">
                            <ul className="list-blog-array list-unstyled">
                                {
                                    this.state.listCategori.map((arrCategori, i) => {
                                        return(
                                            <li key={i}>
                                                <CategoriBlog {...arrCategori}/>
                                            </li>
                                        );
                                    })
                                }
                            </ul>
                        </section>
                        </Col>
                        <Col xs={12} lg={4}>
                            <Search/>
                            <Category/>
                            <RecentArticle/>
                        </Col>
                    </Row>
                </Container>
            </Fragment>
        );
    }
}

export default BlogCategori;