import React, { Component, Fragment } from 'react'
import Helmet from 'react-helmet'

import Hero from '../content/sprinklersystem/Hero'
import Content from '../content/sprinklersystem/Content'
import NewsLetter from '../content/sprinklersystem/Newsletter'
import WOW from 'wowjs'
import WhatsApp from '../content/stiky/WhatsApp'


class SprinklerSystem extends Component {
    componentDidMount() {
        new WOW.WOW({
            live: false
        }).init();
    }
    render() {
        return (
            <Fragment>
                <Helmet>
                    <meta charset="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <meta name="theme-color" content="#000000" />
                    <link rel="icon" href="/favicon-apple.png" sizes="192x192" />
                    <link rel="apple-touch-icon" href="/favicon-apple.png" />
                    <link rel="canonical" href="https://firesystem.co.id/sprinkler-system" />
                    <link rel='shortlink' href='https://firesystem.co.id/sprinkler-system' />
                    {/* Primary Meta Tags */}
                    <title>Sprinkler System, Fire System, distributor alat pemadam</title>
                    <meta name="title" content="Sprinkler System, Fire System, distributor alat pemadam"/>
                    <meta name="description" content="Fire Sprinkler System adalah alat yang dipasang di gedung yang memiliki system instalasi pemadam kebakaran, kami melayani jasa instalasi fire sprinkler system."/>
                    <meta name="keywords" content="fire sprinkler system, fire system, instalasi fire sprinkler, sistem pemadaman otomatis, hydrant system, fire alarm system"/>

                    <meta itemprop="title" content="Sprinkler System, Fire System, distributor alat pemadam"/>
                    <meta itemprop="description" content="Fire Sprinkler System adalah alat yang dipasang di gedung yang memiliki system instalasi pemadam kebakaran, kami melayani jasa instalasi fire sprinkler system."/>
                    <meta itemprop="image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Open Graph / Facebook */}
                    <meta property="og:type" content="website"/>
                    <meta property="og:url" content="https://firesystem.co.id/sprinkler-system"/>
                    <meta property="og:title" content="Sprinkler System, Fire System, distributor alat pemadam"/>
                    <meta property="og:description" content="Fire Sprinkler System adalah alat yang dipasang di gedung yang memiliki system instalasi pemadam kebakaran, kami melayani jasa instalasi fire sprinkler system."/>
                    <meta property="og:image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Twitter */}
                    <meta property="twitter:card" content="summary_large_image"/>
                    <meta property="twitter:url" content="https://firesystem.co.id/sprinkler-system"/>
                    <meta property="twitter:title" content="Sprinkler System, Fire System, distributor alat pemadam"/>
                    <meta property="twitter:description" content="Fire Sprinkler System adalah alat yang dipasang di gedung yang memiliki system instalasi pemadam kebakaran, kami melayani jasa instalasi fire sprinkler system."/>
                    <meta property="twitter:image" content="https://firesystem.co.id/logo/logo.png"/>
                    
                    <meta name="robots" content="index, follow"/>
                    <meta name="googlebot" content="index, follow"/>
                    <meta name="googlebot-news" content="index, follow"/>
                </Helmet>
                <WhatsApp/>
                <Hero/>
                <Content/>
                <NewsLetter/>
            </Fragment>
        )
    }
}

export default SprinklerSystem;