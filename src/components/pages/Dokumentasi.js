import React, { Component, Fragment } from 'react'
import Helmet from 'react-helmet'

import Hero from '../content/dokumentasi/Hero'
import Content from '../content/dokumentasi/Content'
import Gallery from '../content/dokumentasi/Gallery'
import NewsLetter from '../content/dokumentasi/Newsletter'
import WOW from 'wowjs'
import WhatsApp from '../content/stiky/WhatsApp'

class Dokumentasi extends Component {
    componentDidMount() {
        new WOW.WOW({
            live: false
        }).init();
    }
    render() {
        return (
            <Fragment>
                <Helmet>
                    <meta charset="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <meta name="theme-color" content="#000000" />
                    <link rel="icon" href="/favicon-apple.png" sizes="192x192" />
                    <link rel="apple-touch-icon" href="/favicon-apple.png" />
                    <link rel="canonical" href="https://firesystem.co.id/documentation" />
                    <link rel='shortlink' href='https://firesystem.co.id/documentation' />
                    {/* Primary Meta Tags */}
                    <title>Fire Extinguisher, Fire System, alat pemadam api, distributor alat pemadam kebakaran, fire protection berpengalaman</title>
                    <meta name="title" content="Fire Extinguisher, Fire System, alat pemadam api, distributor alat pemadam kebakaran, fire protection berpengalaman"/>
                    <meta name="description" content="PT Rasa nday suksindo melayani jasa perawatan sistem kebakaran yang meliputi fire hydrant, fire suppression systems, fire alarm & fire system untuk proteksi bangunan"/>
                    <meta name="keywords" content="Fire system, alarm system, alat pemadam kebaran, instalasi fire hydrant, fire safety, fire protection berpengalaman, fire hose, fire suit, fire extinguisher, pt rasa ndya suksindo, fire protection terbaik, fire protection berpengalaman, fire system terbaik di indonesia"/>

                    <meta itemprop="title" content="Fire Extinguisher, Fire System, alat pemadam api, distributor alat pemadam kebakaran, fire protection berpengalaman"/>
                    <meta itemprop="description" content="PT Rasa nday suksindo melayani jasa perawatan sistem kebakaran yang meliputi fire hydrant, fire suppression systems, fire alarm & fire system untuk proteksi bangunan"/>
                    <meta itemprop="image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Open Graph / Facebook */}
                    <meta property="og:type" content="website"/>
                    <meta property="og:url" content="https://firesystem.co.id/documentation"/>
                    <meta property="og:title" content="Fire Extinguisher, Fire System, alat pemadam api, distributor alat pemadam kebakaran, fire protection berpengalaman"/>
                    <meta property="og:description" content="PT Rasa nday suksindo melayani jasa perawatan sistem kebakaran yang meliputi fire hydrant, fire suppression systems, fire alarm & fire system untuk proteksi bangunan"/>
                    <meta property="og:image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Twitter */}
                    <meta property="twitter:card" content="summary_large_image"/>
                    <meta property="twitter:url" content="https://firesystem.co.id/documentation"/>
                    <meta property="twitter:title" content="Fire Extinguisher, Fire System, alat pemadam api, distributor alat pemadam kebakaran, fire protection berpengalaman"/>
                    <meta property="twitter:description" content="PT Rasa nday suksindo melayani jasa perawatan sistem kebakaran yang meliputi fire hydrant, fire suppression systems, fire alarm & fire system untuk proteksi bangunan"/>
                    <meta property="twitter:image" content="https://firesystem.co.id/logo/logo.png"/>
                    
                    <meta name="robots" content="index, follow"/>
                    <meta name="googlebot" content="index, follow"/>
                    <meta name="googlebot-news" content="index, follow"/>
                </Helmet>
                <WhatsApp/>
                <Hero/>
                <Content/>
                <Gallery/>
                <NewsLetter/>
            </Fragment>
        );
    }
}

export default Dokumentasi;    
