import React, { Component, Fragment } from 'react'
import Helmet from 'react-helmet'

import Hero from '../content/hydrantequipment/Hero'
import Content from '../content/hydrantequipment/Content'
import Serve from '../content/hydrantequipment/Serve'
import Gallery from '../content/hydrantequipment/GalleryProduct'
import NewsLetter from '../content/hydrantequipment/Newsletter'
import WOW from 'wowjs'
import WhatsApp from '../content/stiky/WhatsApp'

class HydrantEquipment extends Component {
    componentDidMount() {
        new WOW.WOW({
            live: false
        }).init();
    }
    render() {
        return (
            <Fragment>
                <Helmet>
                    <meta charset="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <meta name="theme-color" content="#000000" />
                    <link rel="icon" href="/favicon-apple.png" sizes="192x192" />
                    <link rel="apple-touch-icon" href="/favicon-apple.png" />
                    <link rel="canonical" href="https://firesystem.co.id/hydrant-equipment" />
                    <link rel='shortlink' href='https://firesystem.co.id/hydrant-equipment' />
                    {/* Primary Meta Tags */}
                    <title>Hydrant Equipment, Fire System, Hydran Box, Fire Hydrant Equipment</title>
                    <meta name="title" content="Hydrant Equipment, Fire System, Hydran Box, Fire Hydrant Equipment"/>
                    <meta name="description" content="SUKSINDO menyediakan berbagai macam produk peralatan Hydrant dengan kualitas terbaik, kami juga menawarkan pelayanan jasa instalasi Hydrant system berpengalaman."/>
                    <meta name="keywords" content="Hydrant equipment, box hydrant, fire hose, fire monitor, fire system, kontraktor berpengalaman, distributor alat pemadam kebakaran, instalasi fire alarm, fire engine, suksindo"/>

                    <meta itemprop="title" content="Hydrant Equipment, Fire System, Hydran Box, Fire Hydrant Equipment"/>
                    <meta itemprop="description" content="SUKSINDO menyediakan berbagai macam produk peralatan Hydrant dengan kualitas terbaik, kami juga menawarkan pelayanan jasa instalasi Hydrant system berpengalaman."/>
                    <meta itemprop="image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Open Graph / Facebook */}
                    <meta property="og:type" content="website"/>
                    <meta property="og:url" content="https://firesystem.co.id/hydrant-equipment"/>
                    <meta property="og:title" content="Hydrant Equipment, Fire System, Hydran Box, Fire Hydrant Equipment"/>
                    <meta property="og:description" content="SUKSINDO menyediakan berbagai macam produk peralatan Hydrant dengan kualitas terbaik, kami juga menawarkan pelayanan jasa instalasi Hydrant system berpengalaman."/>
                    <meta property="og:image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Twitter */}
                    <meta property="twitter:card" content="summary_large_image"/>
                    <meta property="twitter:url" content="https://firesystem.co.id/hydrant-equipment"/>
                    <meta property="twitter:title" content="Hydrant Equipment, Fire System, Hydran Box, Fire Hydrant Equipment"/>
                    <meta property="twitter:description" content="SUKSINDO menyediakan berbagai macam produk peralatan Hydrant dengan kualitas terbaik, kami juga menawarkan pelayanan jasa instalasi Hydrant system berpengalaman."/>
                    <meta property="twitter:image" content="https://firesystem.co.id/logo/logo.png"/>
                    
                    <meta name="robots" content="index, follow"/>
                    <meta name="googlebot" content="index, follow"/>
                    <meta name="googlebot-news" content="index, follow"/>
                </Helmet>
                <WhatsApp/>
                <Hero/>
                <Content/>
                <Serve/>
                <Gallery/>
                <NewsLetter/>
            </Fragment>
        );
    }
}

export default HydrantEquipment;    