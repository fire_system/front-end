import React, { Component, Fragment } from 'react'
import Helmet from 'react-helmet'

import Hero from '../content/personalprotectiveequitment/Hero'
import Content from '../content/personalprotectiveequitment/Content'
import Serve from '../content/personalprotectiveequitment/Serve'
import Gallery from '../content/personalprotectiveequitment/GalleryProduct'
import NewsLetter from '../content/personalprotectiveequitment/Newsletter'
import WOW from 'wowjs'
import WhatsApp from '../content/stiky/WhatsApp'

class PersonalProtectiveEquipment extends Component {
    componentDidMount() {
        new WOW.WOW({
            live: false
        }).init();
    }
    render() {
        return (
            <Fragment>
                <Helmet>
                    <meta charset="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <meta name="theme-color" content="#000000" />
                    <link rel="icon" href="/favicon-apple.png" sizes="192x192" />
                    <link rel="apple-touch-icon" href="/favicon-apple.png" />
                    <link rel="canonical" href="https://firesystem.co.id/personal-protective-equipment" />
                    <link rel='shortlink' href='https://firesystem.co.id/personal-protective-equipment' />
                    {/* Primary Meta Tags */}
                    <title>Personal Protective Equipment, Fire System, Fire Protection</title>
                    <meta name="title" content="Personal Protective Equipment, Fire System, Fire Protection"/>
                    <meta name="description" content="Firesystem.co.id kami menyediakan perlengkapkan personal protective equipment serta menyediakan berbagai macam alat perlindungan kebakaran yang sudah terbukti."/>
                    <meta name="keywords" content="System instalasi kebakaran, perlengkapan pemadam kebakaran, fire system, fire alarm system, fire protection, Personal protective equipment, suksindo"/>

                    <meta itemprop="title" content="Personal Protective Equipment, Fire System, Fire Protection"/>
                    <meta itemprop="description" content="Firesystem.co.id kami menyediakan perlengkapkan personal protective equipment serta menyediakan berbagai macam alat perlindungan kebakaran yang sudah terbukti."/>
                    <meta itemprop="image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Open Graph / Facebook */}
                    <meta property="og:type" content="website"/>
                    <meta property="og:url" content="https://firesystem.co.id/personal-protective-equipment"/>
                    <meta property="og:title" content="Personal Protective Equipment, Fire System, Fire Protection"/>
                    <meta property="og:description" content="Firesystem.co.id kami menyediakan perlengkapkan personal protective equipment serta menyediakan berbagai macam alat perlindungan kebakaran yang sudah terbukti."/>
                    <meta property="og:image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Twitter */}
                    <meta property="twitter:card" content="summary_large_image"/>
                    <meta property="twitter:url" content="https://firesystem.co.id/personal-protective-equipment"/>
                    <meta property="twitter:title" content="Personal Protective Equipment, Fire System, Fire Protection"/>
                    <meta property="twitter:description" content="Firesystem.co.id kami menyediakan perlengkapkan personal protective equipment serta menyediakan berbagai macam alat perlindungan kebakaran yang sudah terbukti."/>
                    <meta property="twitter:image" content="https://firesystem.co.id/logo/logo.png"/>
                    
                    <meta name="robots" content="index, follow"/>
                    <meta name="googlebot" content="index, follow"/>
                    <meta name="googlebot-news" content="index, follow"/>
                </Helmet>
                <WhatsApp/>
                <Hero/>
                <Content/>
                <Serve/>
                <Gallery/>
                <NewsLetter/>
            </Fragment>
        );
    }
}

export default PersonalProtectiveEquipment;    