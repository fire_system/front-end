import React, { Component, Fragment } from 'react'
import Helmet from 'react-helmet'
import Hero from '../content/about/Hero'
import Detail from '../content/about/Detail'
import Paralax from '../content/about/Paralax'
import VisiMisi from '../content/about/VisiMisi'
import Newsletter from '../content/about/Newsletter'
import WOW from 'wowjs'
import WhatsApp from '../content/stiky/WhatsApp'



class About extends Component {
    componentDidMount() {
        new WOW.WOW({
            live: false
        }).init();
    }
    render() {
        return (
            <Fragment>
                <Helmet>
                    <meta charset="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <meta name="theme-color" content="#000000" />
                    <link rel="icon" href="/favicon-apple.png" sizes="192x192" />
                    <link rel="apple-touch-icon" href="/favicon-apple.png" />
                    <link rel="canonical" href="https://firesystem.co.id/about" />
                    <link rel='shortlink' href='https://firesystem.co.id/about' />
                    {/* Primary Meta Tags */}
                    <title>Fire System, Pt Rasa Nday Suksindo, Fire Alarm System, Fire Protection, Pydrant System</title>
                    <meta name="title" content="Fire System, Pt Rasa Nday Suksindo, Fire Alarm System, Fire Protection, Pydrant System"/>
                    <meta name="description" content="Suksindo memproduksi Fire Extinguisher dengan merk PRONAAR meliputi Fire System di antaranya penjualan dan Instalasi Fire Alarm, Fire Hydrant, Fire Suppression System."/>
                    <meta name="keywords" content="jual produk alat pemadam kebakaran, jasa instalasi fire system, tabung pemadam api, alat kebakaran, fire system, alarm system, fire protection terbaik, hydrant system."/>

                    <meta itemprop="title" content="Fire System, Pt Rasa Nday Suksindo, Fire Alarm System, Fire Protection, Pydrant System"/>
                    <meta itemprop="description" content="Suksindo memproduksi Fire Extinguisher dengan merk PRONAAR meliputi Fire System di antaranya penjualan dan Instalasi Fire Alarm, Fire Hydrant, Fire Suppression System."/>
                    <meta itemprop="image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Open Graph / Facebook */}
                    <meta property="og:type" content="website"/>
                    <meta property="og:url" content="https://firesystem.co.id/about"/>
                    <meta property="og:title" content="Fire System, Pt Rasa Nday Suksindo, Fire Alarm System, Fire Protection, Pydrant System"/>
                    <meta property="og:description" content="Suksindo memproduksi Fire Extinguisher dengan merk PRONAAR meliputi Fire System di antaranya penjualan dan Instalasi Fire Alarm, Fire Hydrant, Fire Suppression System."/>
                    <meta property="og:image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Twitter */}
                    <meta property="twitter:card" content="summary_large_image"/>
                    <meta property="twitter:url" content="https://firesystem.co.id/about"/>
                    <meta property="twitter:title" content="Fire System, Pt Rasa Nday Suksindo, Fire Alarm System, Fire Protection, Pydrant System"/>
                    <meta property="twitter:description" content="Suksindo memproduksi Fire Extinguisher dengan merk PRONAAR meliputi Fire System di antaranya penjualan dan Instalasi Fire Alarm, Fire Hydrant, Fire Suppression System."/>
                    <meta property="twitter:image" content="https://firesystem.co.id/logo/logo.png"/>
                    
                    <meta name="robots" content="index, follow"/>
                    <meta name="googlebot" content="index, follow"/>
                    <meta name="googlebot-news" content="index, follow"/>
                </Helmet>
                <WhatsApp/>
                <Hero/>
                <Detail/>
                <Paralax/>
                <VisiMisi/>
                <Newsletter/>
            </Fragment>
        )
    }
}

export default About;