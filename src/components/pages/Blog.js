import React, { Component, Fragment } from 'react'
import Helmet from 'react-helmet'
import { Container, Row, Col } from 'react-bootstrap'

import Hero from '../content/blog/Hero'
import Content from '../content/blog/Content'

import Search from '../content/blog/Search'
import Category from '../content/blog/Category'
import RecentArticle from '../content/blog/RecentArticle'
import WOW from 'wowjs'
import WhatsApp from '../content/stiky/WhatsApp'

class Blog extends Component {
    componentDidMount() {
        new WOW.WOW({
            live: false
        }).init();
    }
    render() {
        return (
            <Fragment>
                <Helmet>
                    <meta charset="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <meta name="theme-color" content="#000000" />
                    <link rel="icon" href="/favicon-apple.png" sizes="192x192" />
                    <link rel="apple-touch-icon" href="/favicon-apple.png" />
                    <link rel="canonical" href="https://firesystem.co.id/blog" />
                    <link rel='shortlink' href='https://firesystem.co.id/blog' />
                    {/* Primary Meta Tags */}
                    <title>PT Rasa Nday Suksindo, Fire Protection, Fire Safety, Fire System, Fire Alarm System</title>
                    <meta name="title" content="PT Rasa Nday Suksindo, Fire Protection, Fire Safety, Fire System, Fire Alarm System"/>
                    <meta name="description" content="Produk dan jasa alat pemadam kebakaran meliputi Fire System di antaranya penjualan dan Instalasi Produk Fire Alarm, Fire Hydrant, Fire Suppression System."/>
                    <meta name="keywords" content="jual produk alat pemadam kebakaran, jasa instalasi fire system, tabung pemadam api, alat kebakaran, fire system, alarm system, fire protection terbaik, hydrant system"/>

                    <meta itemprop="title" content="PT Rasa Nday Suksindo, Fire Protection, Fire Safety, Fire System, Fire Alarm System"/>
                    <meta itemprop="description" content="Produk dan jasa alat pemadam kebakaran meliputi Fire System di antaranya penjualan dan Instalasi Produk Fire Alarm, Fire Hydrant, Fire Suppression System."/>
                    <meta itemprop="image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Open Graph / Facebook */}
                    <meta property="og:type" content="website"/>
                    <meta property="og:url" content="https://firesystem.co.id/blog"/>
                    <meta property="og:title" content="PT Rasa Nday Suksindo, Fire Protection, Fire Safety, Fire System, Fire Alarm System"/>
                    <meta property="og:description" content="Produk dan jasa alat pemadam kebakaran meliputi Fire System di antaranya penjualan dan Instalasi Produk Fire Alarm, Fire Hydrant, Fire Suppression System."/>
                    <meta property="og:image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Twitter */}
                    <meta property="twitter:card" content="summary_large_image"/>
                    <meta property="twitter:url" content="https://firesystem.co.id/blog"/>
                    <meta property="twitter:title" content="PT Rasa Nday Suksindo, Fire Protection, Fire Safety, Fire System, Fire Alarm System"/>
                    <meta property="twitter:description" content="Produk dan jasa alat pemadam kebakaran meliputi Fire System di antaranya penjualan dan Instalasi Produk Fire Alarm, Fire Hydrant, Fire Suppression System."/>
                    <meta property="twitter:image" content="https://firesystem.co.id/logo/logo.png"/>
                    
                    <meta name="robots" content="index, follow"/>
                    <meta name="googlebot" content="index, follow"/>
                    <meta name="googlebot-news" content="index, follow"/>
                </Helmet>
                <WhatsApp/>
                <Hero/>
                <Container>
                    <Row>
                        <Col xs={12} lg={8}>
                            <Content/>
                        </Col>
                        <Col xs={12} lg={4}>
                            <Search/>
                            <Category/>
                            <RecentArticle/>
                        </Col>
                    </Row>
                </Container>
            </Fragment>
        )
    }
}

export default Blog;