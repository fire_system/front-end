import React, { Component, Fragment } from 'react';

import Hero from '../content/termcondition/Hero'
import Content from '../content/termcondition/Content'
import WhatsApp from '../content/stiky/WhatsApp'

import Helmet from 'react-helmet'

class TermCondition extends Component {
    render() {
        return (
            <Fragment>
                <Helmet>
                    <meta charset="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <meta name="theme-color" content="#000000" />
                    <link rel="icon" href="/favicon-apple.png" sizes="192x192" />
                    <link rel="apple-touch-icon" href="/favicon-apple.png" />
                    <link rel="canonical" href="https://firesystem.co.id/" />
                    <link rel='shortlink' href='https://firesystem.co.id/' />
                    {/* Primary Meta Tags */}
                    <title>PT Rasa Nday Suksindo, Fire Protection, Fire Safety, Fire System, Fire Alarm System</title>
                    <meta name="title" content="PT Rasa Nday Suksindo, Fire Protection, Fire Safety, Fire System, Fire Alarm System"/>
                    <meta name="description" content="Firesystem dari PT Rasa nday suksindo bergerak di bidang distribusi perlengkapan kebakaran serta jasa kontraktor dan perawatan sistem kebakaran meliputi firesystem."/>
                    <meta name="keywords" content="Fire system, alarm system, alat pemadam kebaran, instalasi fire hydrant, fire safety, fire protection berpengalaman, fire hose, fire suit, fire extinguisher, pt rasa ndya suksindo, fire protection terbaik, fire protection berpengalaman"/>

                    <meta itemprop="title" content="PT Rasa Nday Suksindo, Fire Protection, Fire Safety, Fire System, Fire Alarm System"/>
                    <meta itemprop="description" content="Firesystem dari PT Rasa nday suksindo bergerak di bidang distribusi perlengkapan kebakaran serta jasa kontraktor dan perawatan sistem kebakaran meliputi firesystem."/>
                    <meta itemprop="image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Open Graph / Facebook */}
                    <meta property="og:type" content="website"/>
                    <meta property="og:url" content="https://firesystem.co.id/"/>
                    <meta property="og:title" content="PT Rasa Nday Suksindo, Fire Protection, Fire Safety, Fire System, Fire Alarm System"/>
                    <meta property="og:description" content="Firesystem dari PT Rasa nday suksindo bergerak di bidang distribusi perlengkapan kebakaran serta jasa kontraktor dan perawatan sistem kebakaran meliputi firesystem."/>
                    <meta property="og:image" content="https://firesystem.co.id/logo/logo.png"/>

                    {/* Twitter */}
                    <meta property="twitter:card" content="summary_large_image"/>
                    <meta property="twitter:url" content="https://firesystem.co.id/"/>
                    <meta property="twitter:title" content="PT Rasa Nday Suksindo, Fire Protection, Fire Safety, Fire System, Fire Alarm System"/>
                    <meta property="twitter:description" content="Firesystem dari PT Rasa nday suksindo bergerak di bidang distribusi perlengkapan kebakaran serta jasa kontraktor dan perawatan sistem kebakaran meliputi firesystem."/>
                    <meta property="twitter:image" content="https://firesystem.co.id/logo/logo.png"/>
                    
                    <meta name="robots" content="index, follow"/>
                    <meta name="googlebot" content="index, follow"/>
                    <meta name="googlebot-news" content="index, follow"/>
                </Helmet>
                <WhatsApp/>
                <Hero/>
                <Content/>
            </Fragment>
        );
    }
}

export default TermCondition;