import React, { Fragment, useEffect } from 'react';
import { Container, Navbar, Nav, NavDropdown } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';

function Header() {
    const isSticky = (e) => {
		const header = document.querySelector("header");
		const scrollTop = window.scrollY;
		scrollTop >= 10
			? header.classList.add("sticky")
			: header.classList.remove("sticky");
	};

	useEffect(() => {
		window.addEventListener("scroll", isSticky);
		return () => {
			window.removeEventListener("scroll", isSticky);
		};

        
    });

    const toggleClass = () => {
        const iconBar = document.querySelector("i#iconBars");
        iconBar.classList.toggle("fa-times");
        const menu = document.querySelector("div#basic-navbar-nav");
        menu.classList.toggle("show");
    }
    return (
        <Fragment>
            <header className="main-header">
                <Navbar expand="lg">
                    <div className="navbar-blur"></div>
                    <Container>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" onClick={toggleClass}><i className="fas fa-bars" id="iconBars"></i></Navbar.Toggle>
                        <Navbar.Brand href="/"><img className="img-fluid" src="/logo/logo1.png" alt="Logo Suksindo" /></Navbar.Brand>
                        <Navbar.Collapse className="justify-content-end d-none d-lg-block">
                            <Nav as="ul" id="navigation">
                                <li className="nav-item dropdown">
                                    <a href="/#" className="nav-link">Fire Fighting System</a>
                                    <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <NavLink to="/hydrant-system" activeClassName="active" className="nav-link dropdown-item">Hydrant System</NavLink>
                                        <NavLink to="/sprinkler-system" activeClassName="active" className="nav-link dropdown-item">Sprinkler System</NavLink>
                                    </div>
                                </li>
                                <li className="nav-item dropdown">
                                    <a href="/#" className="nav-link">Fire Protection</a>
                                    <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <NavLink to="/fire-extinguisher-apar-apab" activeClassName="active" className="nav-link dropdown-item">Fire Extinguisher (APAR/APAB)</NavLink>
                                        <NavLink to="/personal-protective-equipment" activeClassName="active" className="nav-link dropdown-item">Personal Protective Equipment</NavLink>
                                        <NavLink to="/hydrant-equipment" activeClassName="active" className="nav-link dropdown-item">Hydrant Equipment</NavLink>
                                    </div>
                                </li>
                                <li className="nav-item dropdown">
                                    <a href="/#" className="nav-link">Maintenance</a>
                                    <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <NavLink to="/fire-supression-system" activeClassName="active" className="nav-link dropdown-item">Fire Supression System</NavLink>
                                        <NavLink to="/alarm-system" activeClassName="active" className="nav-link dropdown-item">Alarm System</NavLink>
                                    </div>
                                </li>
                                <li className="nav-item">
                                    <NavLink to="/ourcustomers" activeClassName="active" className="nav-link">Our Customers</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink to="/dokumentasi" activeClassName="active" className="nav-link">Documentation</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink to="/about" activeClassName="active" className="nav-link">About Us</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink to="/blog" activeClassName="active" className="nav-link">Blog</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink to="/contact" activeClassName="active" className="nav-link">Contact Us</NavLink>
                                </li>
                            </Nav>
                            <div className="navbar-sosmed d-block d-lg-none mt-5">
                                <ul className="list-unstyled d-flex">
                                    <li>
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=suksindo.com/" className="icon-fb" title="Facebook">
                                            <span className="d-none">Facebook</span>
                                            <i className="fab fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/" className="icon-ig" title="Instagram">
                                            <span className="d-none">Instagram</span>
                                            <i className="fab fa-instagram"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </Navbar.Collapse>
                        <div className="mobile_menu d-block d-lg-none" id="basic-navbar-nav" >
                            <Nav className="mr-auto">
                                <NavDropdown title="Fire Fighting System" id="basic-nav-dropdown">
                                    <NavDropdown.Item href="/hydrant-system">Hydrant System</NavDropdown.Item>
                                    <NavDropdown.Item href="/sprinkler-system">Sprinkler System</NavDropdown.Item>
                                </NavDropdown>
                                <NavDropdown title="Fire Protection" id="basic-nav-dropdown">
                                    <NavDropdown.Item href="/fire-extinguisher-apar-apab">Fire Extinguisher (APAR/APAB)</NavDropdown.Item>
                                    <NavDropdown.Item href="/personal-protective-equipment">Personal Protective Equipment</NavDropdown.Item>
                                    <NavDropdown.Item href="/hydrant-equipment">Hydrant Equipment</NavDropdown.Item>
                                </NavDropdown>
                                <NavDropdown title="Maintenance" id="basic-nav-dropdown">
                                    <NavDropdown.Item href="/fire-supression-system">Fire Supression System</NavDropdown.Item>
                                    <NavDropdown.Item href="/alarm-system">Alarm System</NavDropdown.Item>
                                </NavDropdown>
                                <Nav.Link href="/ourcustomers">Our Customers</Nav.Link>
                                <Nav.Link href="/dokumentasi">Documentation</Nav.Link>
                                <Nav.Link href="/about">About Us</Nav.Link>
                                <Nav.Link href="/blog">Blog</Nav.Link>
                                <Nav.Link href="/contact">Contact Us</Nav.Link>
                            </Nav>
                        </div>
                    </Container>
                </Navbar>
            </header>
        </Fragment>
    )
}
export default Header;