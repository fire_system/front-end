import React, { Fragment } from 'react';
import { Container, Row, Col } from 'react-bootstrap';

function Footer() {
    return (
        <Fragment>
            <footer className="main-footer">
                <div className="d-lg-block d-none dots">
                    <span style={{ backgroundImage: 'url(/img/dots.png)' }}></span>
                    <span style={{ backgroundImage: 'url(/img/dots.png)' }}></span>
                    <span style={{ backgroundImage: 'url(/img/dots.png)' }}></span>
                    <span style={{ backgroundImage: 'url(/img/dots.png)' }}></span>
                </div>
                <Container>
                    <Row>
                        <Col xs={12} md={12} lg={3}>
                            <div className="footer-logo">
                                <a href="/"><img className="img-fluid" src="/logo/logo.png" alt="logo suksindo" /></a>
                            </div>
                            <hr/>
                            <div className="footer-sosmed">
                                <ul className="list-unstyled d-flex align-items-center justify-content-center my-2">
                                    <li>
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=suksindo.com/" className="icon-fb"  title="Facebook">
                                            <span className="d-none">Facebook</span>
                                            <i className="fab fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/" className="icon-ig" title="Instagram">
                                            <span className="d-none">Instagram</span>
                                            <i className="fab fa-instagram"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </Col>
                        <Col xs={12} md={12} lg={2}>
                            <div className="footer-information">
                                <h3 className="title">
                                    Information
                                </h3>
                                <ul className="list-unstyled">
                                    <li><a href="/">Home</a></li>
                                    <li><a href="/about">About Us</a></li>
                                    <li><a href="/dokumentasi">Documentation</a></li>
                                    <li><a href="/privacypolicy">Privacy Policy</a></li>
                                    <li><a href="/termcondition">Term & Condition</a></li>
                                </ul>
                            </div>
                        </Col>
                        <Col xs={12} md={12} lg={4}>
                            <div className="footer-service">
                                <h3 className="title">
                                    Services
                                </h3>
                                <ul className="list-unstyled">
                                    <li><a href="/hydrant-system">Hydrant System</a></li>
                                    <li><a href="/sprinkler-system">Sprinkler System</a></li>
                                    <li><a href="/fire-extinguisher-apar-apab">Fire Extinguisher (APAR/APAB)</a></li>
                                    <li><a href="/personal-protective-equipment">Personal Protective Equipment</a></li>
                                    <li><a href="/hydrant-equipment">Hydrant Equipment</a></li>
                                    <li><a href="/fire-supression-system">Fire Supression System</a></li>
                                    <li><a href="/alarm-system">Alarm System</a></li>
                                </ul>
                            </div>
                        </Col>
                        <Col xs={12} md={12} lg={3}>
                            <div className="footer-contact">
                                <h3 className="title">
                                    Contact
                                </h3>
                                <ul className="list-unstyled">
                                    <li>
                                        <i className="fas fa-map-marker"></i>
                                        <p>Sedayu Square rukan blok C No 36, Cengkareng Barat</p>
                                    </li>
                                    <li>
                                        <i className="fas fa-mobile-alt"></i>
                                        <p>0821-8232-4007</p>
                                    </li>
                                    <li>
                                        <i className="fas fa-envelope"></i>
                                        <p>info@firesystem.co.id</p>
                                    </li>
                                </ul>
                            </div>
                        </Col>
                    </Row>
                </Container>
                <div className="footer-bottom">
                    <p>PT Rasa Nday Suksindo © Copyright 2021 - Powered by Imaginative</p>
                </div>
            </footer>
        </Fragment>
    );
}

export default Footer;