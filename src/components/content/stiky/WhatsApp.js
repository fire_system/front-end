import React, { Component, Fragment } from 'react'
import ReactWhatsapp from 'react-whatsapp'

class WhatsApp extends Component {
    render() {
        return (
            <Fragment>
                <ReactWhatsapp number="+6282182324007" className="float">
                    <img className="img-fluid" src="/logo/icon-whatsapp.png" alt="Logo WhatsApp" />
                </ReactWhatsapp>
            </Fragment>
        );
    }
}

export default WhatsApp;