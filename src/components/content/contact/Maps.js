import React, { Component, Fragment } from 'react'
import { Container } from 'react-bootstrap'
import Iframe from 'react-iframe'

class Maps extends Component {
    render() {
        return(
            <Fragment>
                <section className="maps-area wow fadeIn" data-wow-delay="1s">
                    <Container fluid className="p-0">
                        <div className="d-none d-lg-block">
                            <span className="rectangle1" style={{ backgroundImage: 'url(/img/Rectangle.png)' }}></span>
                            <span className="ellipse" style={{ backgroundImage: 'url(/img/Ellipse.png)' }}></span>
                            <span className="rectangle" style={{ backgroundImage: 'url(/img/Rectangle.png)' }}></span>
                        </div>
                        <Iframe className="maps-kantor" tabIndex="0" allowFullScreen={true} aria-hidden="false" style={{border: "0"}} frameBorder="0" width="1496" height="675" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d18121.033454814104!2d106.72373012555227!3d-6.1334169809881205!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x78dfff2c32e3748c!2sRukan%20Sedayu%20Square!5e0!3m2!1sid!2sid!4v1611236064608!5m2!1sid!2sid"/>
                    </Container>
                </section>
            </Fragment>
        );
    }
}

export default Maps;