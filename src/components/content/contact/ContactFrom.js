import React, { Fragment } from 'react'
import * as emailjs from 'emailjs-com'
import swal from 'sweetalert'
import { Container, Row, Col, Form, Button } from 'react-bootstrap'

function ContactFrom() {
    function sendEmail(e) {
        e.preventDefault();

        emailjs.sendForm('gmail', 'template_suksindo', e.target, 'user_YzQ6Tq8YSQP4JDI6nzmku')
        .then((result) => {
            console.log(result.text);
            swal(result.text, "Email Berhasil Terkirim", "success");
        }, (error) => {
            console.log(error.text);
        })
        e.target.reset()
    }
    return(
        <Fragment>
            <section className="form-contact_area">
                <Container>
                    <div className="d-none d-lg-block">
                        <span className="polygon" style={{ backgroundImage: 'url(/img/Polygon.png)' }}></span>
                        <span className="ellipse" style={{ backgroundImage: 'url(/img/Ellipse.png)' }}></span>
                        <span className="rectangle" style={{ backgroundImage: 'url(/img/Rectangle.png)' }}></span>
                    </div>
                    <Row>
                        <Col xs={12}>
                            <div className="section-heading">
                                <h2 className="title wow fadeIn" data-wow-delay=".5s">Hubungi Kami</h2>
                            </div>
                        </Col>
                        <Col xs={12} lg={6} className="wow bounceInLeft" data-wow-delay=".5s">
                            <div className="section-heading">
                                <p className="sub">Apabila membutuhkan Info lainnya silahkan hubungi Kami</p>
                            </div>
                            <div className="media contact-info">
                                <span className="contact-info__icon">
                                    <i className="fas fa-map-marker-alt"></i>
                                </span>
                                <div className="media-body">
                                    <h3>Letak Kantor Kami :</h3>
                                    <p>Sedayu Square Rukan Blok C No 36,<br/>RT/RW : 01/012 Cengkareng Barat.<br/>11730</p>
                                </div>
                            </div>
                            <div className="media contact-info">
                                <span className="contact-info__icon">
                                    <i className="fas fa-envelope"></i>
                                </span>
                                <div className="media-body">
                                    <h3>Alamat Email Kami :</h3>
                                    <p>info@firesystem.co.id</p>
                                </div>
                            </div>
                            <div className="media contact-info">
                                <span className="contact-info__icon">
                                    <i className="fas fa-phone-alt"></i>
                                </span>
                                <div className="media-body">
                                    <h3>Telepon Kami :</h3>
                                    <p>(021) 22553191</p>
                                    <p>0821-8232-4007</p>
                                </div>
                            </div>
                        </Col>
                        <Col xs={12} lg={6} className="wow bounceInRight" data-wow-delay=".5s">
                            <p className="sub font-weight-bold">Silahkan email kami untuk pertanyaan terkait produk kami</p>
                            <Form className="formContact" onSubmit={sendEmail}>
                                <Row>
                                    <Col md={6}>
                                        <Form.Group>
                                            <Form.Control type="text" name="name" placeholder="Nama*" />
                                        </Form.Group>
                                        <Form.Group>
                                            <Form.Control type="text" name="phone" placeholder="Telepon*" />
                                        </Form.Group>
                                    </Col>
                                    <Col md={6}>
                                        <Form.Group>
                                            <Form.Control type="email" name="email" placeholder="Email*" />
                                        </Form.Group>
                                        <Form.Group>
                                            <Form.Control type="text" name="subject" placeholder="Subyek*" />
                                        </Form.Group>
                                    </Col>
                                    <Col md={12}>
                                        <Form.Control as="textarea" name="message" rows={5} placeholder="Tuliskan pesan Anda*" />
                                    </Col>
                                    <Button type="submit" className="btn btn-contact mx-3">Kirim Pesan</Button>
                                </Row>
                            </Form>
                        </Col>
                    </Row>
                </Container>
            </section>
        </Fragment>
    );
}

export default ContactFrom;