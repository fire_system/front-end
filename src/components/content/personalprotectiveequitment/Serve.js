import React, { Component, Fragment } from 'react'
import { Container, Row, Col } from 'react-bootstrap'

class Serve extends Component {
    render() {
        return (
            <Fragment>
                <section className="serve-area wow bounceIn" data-wow-delay=".5s">
                    <Container>
                        <Row>
                            <Col xs={12} lg={4} className="my-3 my-lg-5">
                                <div className="icon">
                                    <span>F</span>
                                </div>
                                <div className="detail">
                                    <p>Fire Suit</p>
                                </div>
                            </Col>
                            <Col xs={12} lg={4} className="my-3 my-lg-5">
                                <div className="icon">
                                    <span>F</span>
                                </div>
                                <div className="detail">
                                    <p>Fireman Boot</p>
                                </div>
                            </Col>
                            <Col xs={12} lg={4} className="my-3 my-lg-5">
                                <div className="icon">
                                    <span>F</span>
                                </div>
                                <div className="detail">
                                    <p>Fireman Helmet</p>
                                </div>
                            </Col>
                            <Col xs={12} lg={4} className="my-3 my-lg-5">
                                <div className="icon">
                                    <span>S</span>
                                </div>
                                <div className="detail">
                                    <p>Self Contained Breathing Apparatus</p>
                                </div>
                            </Col>
                            <Col xs={12} lg={4} className="my-3 my-lg-5">
                                <div className="icon">
                                    <span>E</span>
                                </div>
                                <div className="detail">
                                    <p>Emergency Breathing Apparatus</p>
                                </div>
                            </Col>
                            <Col xs={12} lg={4} className="my-3 my-lg-5">
                                <div className="icon">
                                    <span>F</span>
                                </div>
                                <div className="detail">
                                    <p>Fire Approach and Fire Entry Suit</p>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </Fragment>
        );
    }
}

export default Serve;