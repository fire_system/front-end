import React, { Component, Fragment } from 'react'
import { Container } from 'react-bootstrap'
import OwlCarousel from 'react-owl-carousel'

class GalleryProduct extends Component {
    render() {
        const options = {
            margin: 30,
            loop: true,
            navElement: 'div',
            dots: true,
            nav: false,
            autoplay: true,
            smartSpeed: 2000,
            responsive: {
                0: { items: 1 },
                600: { items: 2 },
                1366: { items: 3 }
            }
        }
        return (
            <Fragment>
                <section className="galleryproduct-area wow bounceInDown" data-wow-delay=".5s">
                    <Container>
                        <h2 className="">Gallery Product</h2>
                        <OwlCarousel className="my-3 my-lg-5 owl-theme" {...options}>
                            <div className="item">
                                <img className="img-fluid" src="/img/ppe/gallery/emergency_fire_fighting_fire_system_suksindo.png" alt="emergency fire fighting fire system suksindo"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ppe/gallery/fire_extinguisher_fire_hose_firesystem_suksindo.png" alt="fire extinguisher fire hose firesystem suksindo"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ppe/gallery/fire_extinguisher_fire_system_suksindo.png" alt="fire extinguisher fire system suksindo"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ppe/gallery/fire_extinguisher_fire-hose_fire_system.png" alt="fire extinguisher fire-hose fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ppe/gallery/fire_protection_berpengalaman.png" alt="fire protection berpengalaman"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ppe/gallery/fire_system_fire_protective.png" alt="fire system fire protective"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ppe/gallery/firefighter_protective_fire_station_suksindo.png" alt="firefighter protective fire station suksindo"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ppe/gallery/personal_protectice_equipment_suksindo_fire_system.png" alt="personal protectice equipment suksindo fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ppe/gallery/personal_protective_fire_system_suksindo.png" alt="personal protective fire system suksindo"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ppe/gallery/personal_protective_fire_system_suksindo_firesystem.png" alt="personal protective fire system suksindo firesystem"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ppe/gallery/suksindo_fire_protection_fire_system.png" alt="suksindo fire protection fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ppe/gallery/suksindo_fire_protective_equipment_fire_system.png" alt="suksindo fire protective equipment fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ppe/gallery/suksindo_fire_protective_fire_system.png" alt="suksindo fire protective fire system"/>
                            </div>
                        </OwlCarousel>
                    </Container>
                </section>
            </Fragment>
        );
    }
}
export default GalleryProduct;