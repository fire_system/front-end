import React, { Component, Fragment } from "react"
import { Container, Row, Col } from 'react-bootstrap'


class Content extends Component {
    render() {
        return (
            <Fragment>
                <section className="protectiveequitment-area">
                    <Container>
                        <div className="d-none d-lg-block">
                            <span className="dot" style={{ backgroundImage: 'url(/img/dots.png)' }}></span>
                            <span className="rectangle" style={{ backgroundImage: 'url(/img/Rectangle.png)' }}></span>
                            <span className="polygon2" style={{ backgroundImage: 'url(/img/Polygon2.png)' }}></span>
                        </div>
                        <Row className="align-items-center">
                            <Col xs={12} lg={6} className="wow bounceInLeft" data-wow-delay=".5s">
                                <h2 className="title">Fire Protection</h2>
                                <h3>Personal Protective Equipment</h3>
                                <p>kelengkapan yang wajib digunakan saat bekerja sesuai bahaya dan risiko kerja untuk menjaga keselamatan pekerja itu sendiri dan orang di sekelilingnya. Kewajiban itu sudah disepakati oleh pemerintah melalui Departement Tenaga Kerja dan Transmigrasi Republik Indonesia</p>
                            </Col>
                            <Col xs={12} lg={6} className="wow bounceInRight" data-wow-delay=".5s">
                                <div className="img-thumb">
                                    <img className="img-fluid" src="/img/ppe/personal_protective_equipment.png" alt="Fire Protection" />
                                </div>
                            </Col>
                            <Col xs={12} lg={12} className="my-lg-5 py-lg-5 wow fadeIn" data-wow-delay=".7s">
                                <p className="text-center detail">Perlengkapkan perlindungan dari api untuk manusia yang dirancang demi mencegah dan melindungi cedera serius atau penyakit yang terjadi akibat kontak langsung dengan api/kebakaran. Kami menyediakan berbagai macam alat perlindungan kebakaran diantaranya seperti :</p>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </Fragment>
        )
    }
}

export default Content;