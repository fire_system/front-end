import React, { Component, Fragment } from 'react';
import { Container, Row, Col } from 'react-bootstrap'


class About extends Component {
    render() {
        return (
            <Fragment>
                <section className="about_area">
                    <Container>
                        <Row className="align-items-center justify-content-center d-flex">
                            <Col md={12} lg={8}  className="order-2 order-lg-1 wow bounceInLeft" data-wow-delay=".7s">
                                <h2 className="title mb-3 mb-lg-5">Tentang PT. RASA NDAY SUKSINDO</h2>
                                <p className="content">
                                    PT. RASA NDAY SUKSINDO merupakan perusahaan yang bergerak dibidang alat pemadam kebakaran, sistem instalasi kebakaran dan perlengkapan pemadam kebakaran. Produk dan jasa yang kami miliki tidak hanya berkisar pada tabung pemadam api atau alat pemadam api ringan saja, melainkan juga meliputi Fire System di antaranya yaitu Penjualan dan Instalasi Produk Fire Alarm, Fire Hydrant, Fire Suppression System dan banyak lagi.
                                </p>
                            </Col>
                            <Col md={12} lg={4} className="order-1 order-lg-2 wow bounceInRight" data-wow-delay=".7s">
                                <div className="about-img">
                                    <div className="overlay">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                    <div className="frame-about">
                                        <img className="img-fluid" src="/img/about-1.png" alt="about" />
                                        {/* <div className="img-bg-about" style={{backgroundImage: 'url(./img/about-home.png)'}}></div> */}
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </Fragment>
        )
    }
}

export default About;