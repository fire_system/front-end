import React, { Component, Fragment } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import ReactPlayer from 'react-player'


class Service extends Component {
    render() {
        return (
            <Fragment>
                <section className="service_area">
                    <Container>
                        <Row className="align-items-center">
                            <Col xs={12} lg={6} md={12} className="order-2 order-lg-1 wow bounceInLeft" data-wow-delay="1.1s">
                                <p className="content my-4">PT. RASA NDAY SUKSINDO merupakan perusahaan yang bergerak dibidang alat pemadam kebakaran, sistem instalasi kebakaran dan perlengkapan pemadam kebakaran. Produk dan jasa yang kami miliki tidak hanya berkisar pada tabung pemadam api atau alat pemadam api ringan saja, melainkan juga meliputi Fire System di antaranya yaitu Penjualan dan Instalasi Produk Fire Alarm, Fire Hydrant, Fire Suppression System dan banyak lagi.</p>
                                <a href="/contact" className="btn btn-service m-1 my-lg-3 active">Hubungi Kami</a>
                            </Col>
                            <Col xs={12} lg={6} md={12} className="order-1 order-lg-2 wow bounceInRight" data-wow-delay="1.1s">
                                <div className="image-service mb-3 mb-lg-0">
                                    <ReactPlayer url="/video/fire_system.mp4" width="100%" height="100%" muted={true} playing={true} controls loop={true}/>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </Fragment>
        );
    }
}

export default Service;