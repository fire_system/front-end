import React, { Component, Fragment } from 'react';
import { Container, Row, Col } from 'react-bootstrap';


class Hero extends Component {
    render() {
        return (
            <Fragment>
                <section className="hero_area my-0" style={{backgroundImage: 'url(./img/hero.png)'}}>
                    <div className="overlay"></div>
                    <Container>
                        <Row className="align-items-center justify-content-center">
                            <Col md={8} className="text-center">
                                <p>Firesystem By</p>
                                <h1 className="text-uppercase">pt. rasa nday suksindo<br/>ALAT PEMADAM KEBAKARAN</h1>
                                <p className="">
                                    Produk dan jasa yang kami miliki tidak hanya berkisar pada tabung pemadam api<br className="d-none d-lg-block"/> atau alat pemadam api ringan saja, melainkan juga meliputi Fire System di<br className="d-none d-lg-block"/> antaranya yaitu Penjualan dan Instalasi Produk Fire Alarm, Fire Hydrant, Fire<br className="d-none d-lg-block"/> Suppression System dan banyak lagi.
                                </p>
                                <div className="mt-2 mt-lg-5">
                                    <a href="/" className="btn btn-hero m-1 m-lg-3 active">Hubungi Kami</a>
                                    {/* <a href="/" className="btn btn-hero m-1 m-lg-3"><i className="fas fa-clipboard-list mr-2"/>Pricelist Kami</a> */}
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </Fragment>
        )
    }
}

export default Hero;