import React, { Component, Fragment } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import OwlCarousel from 'react-owl-carousel';


class Client extends Component {
    render() {
        const responsive = {
            0: { items: 1 },
            600: { items: 2 },
            1000: { items: 4 }
        };
        return (
            <Fragment>
                <section className="client_area overlay wow fadeIn" data-wow-delay="1.3s">
                    <Container>
                        <h2 className="title text-center">Dipercaya oleh banyak Perusahaan Ternama</h2>
                        <Row>
                            <Col xs={12}>
                                <OwlCarousel className="owl-theme owl-style my-5" margin={10} responsive={responsive} dots={true} nav={false} navElement="div" smartSpeed={500} autoplay loop>
                                    <div className="client-item">
                                        {/* <div className="client-logo"> */}
                                            <img className="img-fluid" src="/img/client/clinet-bakrie.png" alt="clinet-bakrie" />
                                        {/* </div> */}
                                    </div>
                                    <div className="client-item">
                                        {/* <div className="client-logo"> */}
                                            <img className="img-fluid" src="/img/client/clinet-mandiri.png" alt="clinet-mandiri" />
                                        {/* </div> */}
                                    </div>
                                    <div className="client-item">
                                        {/* <div className="client-logo"> */}
                                            <img className="img-fluid" src="/img/client/clinet-aviatour.png" alt="clinet-aviatour" />
                                        {/* </div> */}
                                    </div>
                                    <div className="client-item">
                                        {/* <div className="client-logo"> */}
                                            <img className="img-fluid" src="/img/client/clinet-rimba.png" alt="clinet-rimba" />
                                        {/* </div> */}
                                    </div>
                                    <div className="client-item">
                                        {/* <div className="client-logo"> */}
                                            <img className="img-fluid" src="/img/client/clinet-kuri.png" alt="clinet-kuri" />
                                        {/* </div> */}
                                    </div>
                                    <div className="client-item">
                                        {/* <div className="client-logo"> */}
                                            <img className="img-fluid" src="/img/client/clinet-ybtci.png" alt="clinet-ybtci" />
                                        {/* </div> */}
                                    </div>
                                    <div className="client-item">
                                        {/* <div className="client-logo"> */}
                                            <img className="img-fluid" src="/img/client/clinet-mustikaratu.png" alt="clinet-mustikaratu" />
                                        {/* </div> */}
                                    </div>
                                    <div className="client-item">
                                        {/* <div className="client-logo"> */}
                                            <img className="img-fluid" src="/img/client/clinet-hestiwirasakti.png" alt="clinet-hestiwirasakti" />
                                        {/* </div> */}
                                    </div>
                                    <div className="client-item">
                                        {/* <div className="client-logo"> */}
                                            <img className="img-fluid" src="/img/client/clinet-etihad.png" alt="clinet-etihad" />
                                        {/* </div> */}
                                    </div>
                                    <div className="client-item">
                                        {/* <div className="client-logo"> */}
                                            <img className="img-fluid" src="/img/client/clinet-ipc.png" alt="clinet-ipc" />
                                        {/* </div> */}
                                    </div>
                                    <div className="client-item">
                                        {/* <div className="client-logo"> */}
                                            <img className="img-fluid" src="/img/client/clinet-ptlautanluastbk.png" alt="clinet-ptlautanluastbk" />
                                        {/* </div> */}
                                    </div>
                                    <div className="client-item">
                                        {/* <div className="client-logo"> */}
                                            <img className="img-fluid" src="/img/client/clinet-mnctv.png" alt="clinet-mnctv" />
                                        {/* </div> */}
                                    </div>
                                    <div className="client-item">
                                        {/* <div className="client-logo"> */}
                                            <img className="img-fluid" src="/img/client/clinet-synergy.png" alt="clinet-synergy" />
                                        {/* </div> */}
                                    </div>
                                </OwlCarousel>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </Fragment>
        );
    }
}

export default Client