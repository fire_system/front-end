import React, { Component, Fragment } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import OwlCarousel from 'react-owl-carousel';


class Product extends Component {
    render() {
        const options = {
            margin: 50,
            loop: true,
            navElement: 'div',
            dots: true,
            nav: false,
            autoplay: true,
            smartSpeed: 1000,
            items: 1
        }
        return (
            <Fragment>
                <section className="product_area wow fadeIn" data-wow-delay="1s">
                    <h2 className="title text-center pb-4 px-5">Produk Unggulan Kami</h2>
                    <div className="product-slide">
                        <Container>
                            <Row className="align-items-center justify-content-center">
                                <Col xs={12} lg={12}>
                                    <OwlCarousel className="owl-theme owl-style" {...options}>
                                        <div className="item-product">
                                            <div className="product-icon">
                                                <i className="fas fa-fire"/>
                                            </div>
                                            <h3 className="title my-3">Fire Fighting System (produk unggulan)</h3>
                                            <div className="product-content">
                                                <p>PT. RASA NDAY SUKSINDO melayani jasa Instalasi serta testing dan maintenance untuk fasilitas system pemadam pada gedung atau fasilitas lainnya sepert :</p>
                                                <ul className="list-unstyled mt-3">
                                                    <li>1. Hydrant System</li>
                                                    <li>2. Sprinkler System</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="item-product">
                                            <div className="product-icon">
                                                <i className="fas fa-fire-extinguisher"/>
                                            </div>
                                            <h3 className="title my-3">PRONAAR FIRE EXTINGUISHER</h3>
                                            <div className="product-content">
                                                <p>1. PRONAAR Fire Extinguisher merupakan alat pemadam api dengan kemampuan tinggi yang sangat sesuai untuk digunakan berbagai aplikasi seperti industri, gudang, rumah, kantor, restoran, hotel, pertambangan, SPBU dan lain-lain. PRONAAR merupakan fire extinguisher produk dalam negeri yang diproduksikan oleh PT. RASA NDAY SUKSINDO yang telah lulus uji dinas pemadam kebakaran.</p>
                                                <p>2. PRONAAR FIre Extinguisher tersedia dengan berbagai ukuran dan media racun api. selain itu, tabung pemadam kebakaran PRONAAR juga memiliki desain tabung yang menarik dan elegan sehingga dapat mendukung keindahan interior kantor/rumah.</p>
                                            </div>
                                        </div>
                                        <div className="item-product">
                                            <div className="product-icon">
                                                <i className="fas fa-dumpster-fire"></i>
                                            </div>
                                            <h3 className="title my-3">FIRE SUIT PRONAAR</h3>
                                            <div className="product-content">
                                                <p>Baju pemadam kebakaran tahan panas adalah baju pemadam kebakaran lengkap dengan celana yang terbuat dari material tahan panas atau bahan dasar kain tahan panas yang tebal yang berfungsi sebagai pakaian pelindung diri atau baju safety dalam melakukan kegiatan tugas pemadam kebakaran.</p>
                                            </div>
                                        </div>
                                        <div className="item-product">
                                            <div className="product-icon">
                                                <i className="fas fa-hard-hat"/>
                                            </div>
                                            <h3 className="title my-3">FIRE HOSE PRONAAR</h3>
                                            <div className="product-content">
                                                <p>Fire Hose atau Selang Pemadam merupakan salah satu produk yang biasa digunakan oleh tim pemadam kebakaran ketika terjadi kebakaran pada sebuah bangunan atau gedung. selang pemadam api ini termasuk Fire Hydrant Equipment yang penting digunakan untuk memaksimalkan sistem fire hydrant untuk memadamkan kebakaran.Fire Hose PRONAAR telah lulus uji fungsi yang dilakukan oleh dinas pemadam kebakaran Indonesia, sehingga produk kami memiliki kualitas dan mutu yang baik serta terjamin.</p>
                                            </div>
                                        </div>
                                    </OwlCarousel>
                                </Col>
                            </Row>
                        </Container>
                    </div>
                </section>
            </Fragment>
        )
    }
}

export default Product;