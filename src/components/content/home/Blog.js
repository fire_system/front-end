import React, { Component, Fragment } from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import Interweave from 'interweave'
import dateFormat from 'dateformat'

class Blog extends Component {
    state = {
        listBlog: [],
    }
    componentDidMount() {
        this.loadlistBlog()
    }
    loadlistBlog = () => {
        const {  listBlog } = this.state;
        const url = process.env.REACT_APP_API_URL + `/newsapi`
        // console.log(url);
        fetch(url, { headers: { Authorization: 'Bearer '.concat(process.env.REACT_APP_TOKEN) } })
        .then(response => response.json())
        .then(json => this.setState({
            listBlog : [...listBlog, ...json.data.sources],
        }))
    }
    render() {
        return (
            <Fragment>
                <div className="squer"><span></span></div>
                <section className="blog_area wow fadeIn" data-wow-delay="1s">
                    <Container>
                        <h3 className="sub-title text-center">Related Post</h3>
                        <h2 className="title text-center mt-3 mb-5">Popular Blog</h2>
                        <Row>
                            {
                                this.state.listBlog.slice(0, 3).map((blog, i) => {
                                    return(
                                        <Col xs={12} lg={4} md={12} className="my-lg-0 pb-0 my-2 wow bounceInLeft" data-wow-delay="1s" key={i}>
                                            <div className="post-blog">
                                                <div className="single-post-blog">
                                                    <div className="blog-img">
                                                        <a href={'/blog/'+ blog.category.slug + '/' + blog.slug} title={blog.title}>
                                                            <img className="img-fluid" src={"https://dashboard.firesystem.co.id/storage/thumbnail/" + blog.thumbnail} alt={blog.title} />
                                                        </a>
                                                        <div className="blog-meta">
                                                            <span className="blog-date">{ dateFormat(blog.updated_at, "mmmm d, yyyy") }</span>
                                                        </div>
                                                    </div>
                                                    <div className="blog-body">
                                                        <small>{ blog.author } - { blog.category.name }</small>
                                                        <a href={'/blog/'+ blog.category.slug + '/' + blog.slug} title={blog.title}><h3 className="title">{ blog.title }</h3></a>
                                                        {/* <p className="blog-content">Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</p> */}
                                                        <Interweave content={blog.content}/>
                                                    </div>
                                                </div>
                                            </div>
                                        </Col>
                                    );
                                })
                            }
                        {/* {
                            this.state.listBlog.map.slice(0, 3)((blog, i) => {
                                return( */}
                                    
                                {/* );
                            })
                        } */}
                        </Row>
                    </Container>
                </section>
            </Fragment>
        )
    }
}

export default Blog;