import React, { Component, Fragment } from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import OwlCarousel from 'react-owl-carousel'
import ReactWhatsapp from 'react-whatsapp'


class Hero extends Component {
    render() {
        const options = {
            loop: true,
            dots: false,
            nav: false,
            autoplay: true,
            smartSpeed: 1250,
            items: 1
        }
        return (
            <Fragment>
                <section className="hero_area my-0">
                    <OwlCarousel className="owl-theme" {...options}>
                        <div className="item" style={{backgroundImage: 'url(/img/banner/Banner_home_fire_system_01-min.jpg)'}}></div>
                        <div className="item" style={{backgroundImage: 'url(/img/banner/Banner_home_fire_system_02-min.jpg)'}}></div>
                    </OwlCarousel>
                    <div className="overlay">
                        <div className="hero-content wow fadeIn" data-wow-delay=".5s">
                            <Container>
                                <Row className="align-items-center justify-content-center d-flex">
                                    <Col md={8} className="text-center">
                                        <p>Firesystem By</p>
                                        <h1 className="text-uppercase">pt. rasa nday suksindo<br/>ALAT PEMADAM KEBAKARAN</h1>
                                        <p className="">
                                            Produk dan jasa yang kami miliki tidak hanya berkisar pada tabung pemadam api<br className="d-none d-lg-block"/> atau alat pemadam api ringan saja, melainkan juga meliputi Fire System di<br className="d-none d-lg-block"/> antaranya yaitu Penjualan dan Instalasi Produk Fire Alarm, Fire Hydrant, Fire<br className="d-none d-lg-block"/> Suppression System dan banyak lagi.
                                        </p>
                                        <div className="mt-2 mt-lg-5">
                                            {/* <a href="/" className="btn btn-hero m-1 m-lg-3 active">Hubungi Kami</a> */}
                                            <ReactWhatsapp number="+6282182324007" className="btn btn-hero m-1 m-lg-3 active">
                                                Hubungi Kami
                                            </ReactWhatsapp>
                                            {/* <a href="/" className="btn btn-hero m-1 m-lg-3"><i className="fas fa-clipboard-list mr-2"/>Pricelist Kami</a> */}
                                        </div>
                                    </Col>
                                </Row>
                            </Container>
                        </div>
                    </div>
                </section>
            </Fragment>
        )
    }
}

export default Hero;