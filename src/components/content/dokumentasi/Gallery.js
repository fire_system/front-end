import React, { Component, Fragment } from 'react'
import { Container, Row, Col } from 'react-bootstrap'

class Gallery extends Component {
    render() {
        return (
            <Fragment>
                <section className="gallerydokumentasi-area wow fadeIn" data-wow-delay="1s">
                    <Container>
                        <h2 className="">Gallery</h2>
                        <Row id="gallery">
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/alarm_instalasi_susuksindo.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/alarm_instalasi_susuksindo.png">
                                        <img className="img-fluid" src="/img/dokumentasi/alarm_instalasi_susuksindo.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/alat_fire_system.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/alat_fire_system.png">
                                        <img className="img-fluid" src="/img/dokumentasi/alat_fire_system.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/customer_suksindo.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/customer_suksindo.png">
                                        <img className="img-fluid" src="/img/dokumentasi/customer_suksindo.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_alarm.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_alarm.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_alarm.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_alarm_gedung.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_alarm_gedung.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_alarm_gedung.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_alarm_system_suksindo.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_alarm_system_suksindo.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_alarm_system_suksindo.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_contruction.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_contruction.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_contruction.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_extinguisher.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_extinguisher.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_extinguisher.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_hidrandt.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_hidrandt.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_hidrandt.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_hydrant.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_hydrant.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_hydrant.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_hydrant_protection.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_hydrant_protection.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_hydrant_protection.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_protection.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_protection.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_protection.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_protection_berpengalaman.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_protection_berpengalaman.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_protection_berpengalaman.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_protection_instalasi.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_protection_instalasi.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_protection_instalasi.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_protection_rasa_nday_suksindo.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_protection_rasa_nday_suksindo.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_protection_rasa_nday_suksindo.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_protection_suksindo.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_protection_suksindo.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_protection_suksindo.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_safety.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_safety.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_safety.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_sistems.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_sistems.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_sistems.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_system.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_system.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_system.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_system_dari_suksindo.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_system_dari_suksindo.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_system_dari_suksindo.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_system_instalasi.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_system_instalasi.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_system_instalasi.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_system_maintenance.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_system_maintenance.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_system_maintenance.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_system_profesional.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_system_profesional.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_system_profesional.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_system_rasa_nday_suksindo.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_system_rasa_nday_suksindo.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_system_rasa_nday_suksindo.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_system_suksindo.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_system_suksindo.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_system_suksindo.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/fire_system_terbaik.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/fire_system_terbaik.png">
                                        <img className="img-fluid" src="/img/dokumentasi/fire_system_terbaik.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/firesystem.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/firesystem.png">
                                        <img className="img-fluid" src="/img/dokumentasi/firesystem.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/hydrant.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/hydrant.png">
                                        <img className="img-fluid" src="/img/dokumentasi/hydrant.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/hyrant_instalasi_suksindo.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/hyrant_instalasi_suksindo.png">
                                        <img className="img-fluid" src="/img/dokumentasi/hyrant_instalasi_suksindo.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/instalasi_anti_kebakaran.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/instalasi_anti_kebakaran.png">
                                        <img className="img-fluid" src="/img/dokumentasi/instalasi_anti_kebakaran.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/instalasi_fire_alarm.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/instalasi_fire_alarm.png">
                                        <img className="img-fluid" src="/img/dokumentasi/instalasi_fire_alarm.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/instalasi_fire_protection.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/instalasi_fire_protection.png">
                                        <img className="img-fluid" src="/img/dokumentasi/instalasi_fire_protection.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/instalasi_fire_safety.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/instalasi_fire_safety.png">
                                        <img className="img-fluid" src="/img/dokumentasi/instalasi_fire_safety.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/instalasi_fire_system.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/instalasi_fire_system.png">
                                        <img className="img-fluid" src="/img/dokumentasi/instalasi_fire_system.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/instalasi_fire_system_perusahaan.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/instalasi_fire_system_perusahaan.png">
                                        <img className="img-fluid" src="/img/dokumentasi/instalasi_fire_system_perusahaan.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/instalasi_firesystem.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/instalasi_firesystem.png">
                                        <img className="img-fluid" src="/img/dokumentasi/instalasi_firesystem.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/instalasi_jaringan_air.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/instalasi_jaringan_air.png">
                                        <img className="img-fluid" src="/img/dokumentasi/instalasi_jaringan_air.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/instalasi_saluran_air.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/instalasi_saluran_air.png">
                                        <img className="img-fluid" src="/img/dokumentasi/instalasi_saluran_air.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/instalasi_saluran_air_cegah_kebakaran.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/instalasi_saluran_air_cegah_kebakaran.png">
                                        <img className="img-fluid" src="/img/dokumentasi/instalasi_saluran_air_cegah_kebakaran.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/kontruksi_suksindo.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/kontruksi_suksindo.png">
                                        <img className="img-fluid" src="/img/dokumentasi/kontruksi_suksindo.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/nday_suksindo.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/nday_suksindo.png">
                                        <img className="img-fluid" src="/img/dokumentasi/nday_suksindo.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/our_customer_fire_system.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/our_customer_fire_system.png">
                                        <img className="img-fluid" src="/img/dokumentasi/our_customer_fire_system.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/pemasangan_fire_system.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/pemasangan_fire_system.png">
                                        <img className="img-fluid" src="/img/dokumentasi/pemasangan_fire_system.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/pemasangan_pipa_air_gedung.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/pemasangan_pipa_air_gedung.png">
                                        <img className="img-fluid" src="/img/dokumentasi/pemasangan_pipa_air_gedung.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/pembuatan_jalur_fire_system.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/pembuatan_jalur_fire_system.png">
                                        <img className="img-fluid" src="/img/dokumentasi/pembuatan_jalur_fire_system.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/personal_protective.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/personal_protective.png">
                                        <img className="img-fluid" src="/img/dokumentasi/personal_protective.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/pipa_air_gedung.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/pipa_air_gedung.png">
                                        <img className="img-fluid" src="/img/dokumentasi/pipa_air_gedung.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/pipa_fire_system.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/pipa_fire_system.png">
                                        <img className="img-fluid" src="/img/dokumentasi/pipa_fire_system.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/pipa_suksindo.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/pipa_suksindo.png">
                                        <img className="img-fluid" src="/img/dokumentasi/pipa_suksindo.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/pipa_untuk_fire_system.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/pipa_untuk_fire_system.png">
                                        <img className="img-fluid" src="/img/dokumentasi/pipa_untuk_fire_system.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/proses_instalasi_suksindo.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/proses_instalasi_suksindo.png">
                                        <img className="img-fluid" src="/img/dokumentasi/proses_instalasi_suksindo.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/proses_kerja_suksindo.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/proses_kerja_suksindo.png">
                                        <img className="img-fluid" src="/img/dokumentasi/proses_kerja_suksindo.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/proses_pemasangan_fire_protection_dari_suksindo.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/proses_pemasangan_fire_protection_dari_suksindo.png">
                                        <img className="img-fluid" src="/img/dokumentasi/proses_pemasangan_fire_protection_dari_suksindo.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/proyek_suksindo.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/proyek_suksindo.png">
                                        <img className="img-fluid" src="/img/dokumentasi/proyek_suksindo.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/pt_rasa_nday_suksindo_fire_system.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/pt_rasa_nday_suksindo_fire_system.png">
                                        <img className="img-fluid" src="/img/dokumentasi/pt_rasa_nday_suksindo_fire_system.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/rasa_nday_suksindo.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/rasa_nday_suksindo.png">
                                        <img className="img-fluid" src="/img/dokumentasi/rasa_nday_suksindo.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/rns.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/rns.png">
                                        <img className="img-fluid" src="/img/dokumentasi/rns.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/saluran_air_anti_kebakaran.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/saluran_air_anti_kebakaran.png">
                                        <img className="img-fluid" src="/img/dokumentasi/saluran_air_anti_kebakaran.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/saluran_air_fire_system_gedung.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/saluran_air_fire_system_gedung.png">
                                        <img className="img-fluid" src="/img/dokumentasi/saluran_air_fire_system_gedung.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/saluran_air_gedung.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/saluran_air_gedung.png">
                                        <img className="img-fluid" src="/img/dokumentasi/saluran_air_gedung.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/simulasi_fire_system.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/simulasi_fire_system.png">
                                        <img className="img-fluid" src="/img/dokumentasi/simulasi_fire_system.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/suksindo.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/suksindo.png">
                                        <img className="img-fluid" src="/img/dokumentasi/suksindo.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/suksindo_fire_system.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/suksindo_fire_system.png">
                                        <img className="img-fluid" src="/img/dokumentasi/suksindo_fire_system.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                            <Col xs={6} lg={4} md={6} sm={6} data-src="https://firesystem.co.id/img/dokumentasi/suksindo_firesystem.png" className="p-2">
                                <div className="dokumtasi-box">
                                    <a href="https://firesystem.co.id/img/dokumentasi/suksindo_firesystem.png">
                                        <img className="img-fluid" src="/img/dokumentasi/suksindo_firesystem.png" alt="Fire Suppression System"/>
                                    </a>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </Fragment>
        );
    }
}
export default Gallery;