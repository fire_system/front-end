import React, { Component, Fragment } from "react"
import { Container, Row, Col } from 'react-bootstrap'
import ReactPlayer from 'react-player'


class Content extends Component {
    render() {
        return (
            <Fragment>
                <section className="videodokumtasi-area wow fadeIn" data-wow-delay=".5s">
                    <Container>
                        <Row className="align-items-center">
                            <Col xs={12} className="text-center">
                                <ReactPlayer className="videodokumtasi" url="/video/Instalasi_fire_system.mp4" muted={true} playing={true} controls loop={true}/>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </Fragment>
        )
    }
}

export default Content;