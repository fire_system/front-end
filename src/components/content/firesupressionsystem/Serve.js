import React, { Component, Fragment } from 'react'
import { Container, Row, Col } from 'react-bootstrap'

class Serve extends Component {
    render() {
        return (
            <Fragment>
                <section className="serve-area">
                    <Container>
                        <h2 className="">Melayani</h2>
                        <Row>
                            <Col xs={12} lg={3} className="my-3 my-lg-5">
                                <div className="icon">
                                    <span>B</span>
                                </div>
                                <div className="detail">
                                    <p>Box Hydrant</p>
                                </div>
                            </Col>
                            <Col xs={12} lg={3} className="my-3 my-lg-5">
                                <div className="icon">
                                    <span>F</span>
                                </div>
                                <div className="detail">
                                    <p>Fire Hose</p>
                                </div>
                            </Col>
                            <Col xs={12} lg={3} className="my-3 my-lg-5">
                                <div className="icon">
                                    <span>H</span>
                                </div>
                                <div className="detail">
                                    <p>Hose Reel</p>
                                </div>
                            </Col>
                            <Col xs={12} lg={3} className="my-3 my-lg-5">
                                <div className="icon">
                                    <span>F</span>
                                </div>
                                <div className="detail">
                                    <p>Fire Nozzle</p>
                                </div>
                            </Col>
                            <Col xs={12} lg={3} className="my-3 my-lg-5">
                                <div className="icon">
                                    <span>F</span>
                                </div>
                                <div className="detail">
                                    <p>Fire Monitor</p>
                                </div>
                            </Col>
                            <Col xs={12} lg={3} className="my-3 my-lg-5">
                                <div className="icon">
                                    <span>F</span>
                                </div>
                                <div className="detail">
                                    <p>Fire Pump</p>
                                </div>
                            </Col>
                            <Col xs={12} lg={3} className="my-3 my-lg-5">
                                <div className="icon">
                                    <span>F</span>
                                </div>
                                <div className="detail">
                                    <p>Fire Engine</p>
                                </div>
                            </Col>
                            <Col xs={12} lg={3} className="my-3 my-lg-5">
                                <div className="icon">
                                    <span>L</span>
                                </div>
                                <div className="detail">
                                    <p>Lainnya</p>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </Fragment>
        );
    }
}

export default Serve;