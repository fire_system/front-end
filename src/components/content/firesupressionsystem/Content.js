import React, { Component, Fragment } from "react"
import { Container, Row, Col } from 'react-bootstrap'


class Content extends Component {
    render() {
        return (
            <Fragment>
                <section className="firesupressionsys-area">
                    <Container>
                        <Row className="align-items-center">
                            <Col xs={12} lg={6} className="wow bounceInLeft" data-wow-delay=".5s">
                                <h2 className="title">Fire Suppression System</h2>
                                <h3>Apa itu Fire Suppression System ?</h3>
                                <p>Sistem pengindera api atau yang umum dikenal dengan fire alarm system adalah suatu sistem terintegrasi yang didesain dan dibangun untuk mendeteksi adanya gejala kebakaran, untuk kemudian memberi peringatan (warning) dalam sistem evakuasi dan ditindak lanjuti secara otomatis maupun manual dengan sistem instalasi pemadam kebakaran (fire fighting system). Kami PT. RASA NDAY SUKSINDO menyediakan serangkaian lengkap perangkat alarm system seperti Detector (heat and Smoke), Panel System dan Sounder (Bell,Horn).</p>
                            </Col>
                            <Col xs={12} lg={6} className="wow bounceInRight" data-wow-delay=".5s">
                                <div className="img-thumb">
                                    {/* <img className="img-fluid" src="/img/fs/cover_fire_suppression.png" alt="Fire Suppression" /> */}
                                    <img className="img-fluid" src="/img/fss/cover_fire_suppression.png" alt="Apa Itu Hydrant System" />
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </Fragment>
        )
    }
}

export default Content;