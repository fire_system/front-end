import React, { Component, Fragment } from 'react'
import { Container } from 'react-bootstrap'
import OwlCarousel from 'react-owl-carousel'

class GalleryProduct extends Component {
    render() {
        const options = {
            margin: 30,
            loop: true,
            navElement: 'div',
            dots: true,
            nav: false,
            autoplay: true,
            smartSpeed: 2000,
            responsive: {
                0: { items: 1 },
                600: { items: 2 },
                1366: { items: 3 }
            }
        }
        return (
            <Fragment>
                <section className="galleryproduct-area wow fadeIn" data-wow-delay=".5s">
                    <Container>
                        <h2 className="">Gallery Product</h2>
                        <OwlCarousel className="my-3 my-lg-5 owl-theme" {...options}>
                            <div className="item">
                                <img className="img-fluid" src="/img/fss/gallery/fire_suppresion_fire_system.png" alt="fire suppresion fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/fss/gallery/Fire_suppression.png" alt="Fire suppression"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/fss/gallery/fire_suppression_fire_system.png" alt="fire suppression fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/fss/gallery/fire_suppression_suksindo.png" alt="fire suppression suksindo"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/fss/gallery/fire_suppression_system.png" alt="fire suppression system"/>
                            </div>
                        </OwlCarousel>
                    </Container>
                </section>
            </Fragment>
        );
    }
}
export default GalleryProduct;