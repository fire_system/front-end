import React, { Component, Fragment } from 'react'
import { Container } from 'react-bootstrap'
import OwlCarousel from 'react-owl-carousel'

class GalleryProduct extends Component {
    render() {
        const options = {
            margin: 30,
            loop: true,
            navElement: 'div',
            dots: true,
            nav: false,
            autoplay: true,
            smartSpeed: 2000,
            responsive: {
                0: { items: 1 },
                600: { items: 2 },
                1366: { items: 3 }
            }
        }
        return (
            <Fragment>
                <section className="galleryproduct-area wow bounceInDown" data-wow-delay=".5s">
                    <Container>
                        <h2 className="">Gallery Product</h2>
                        <OwlCarousel className="my-3 my-lg-5 owl-theme" {...options}>
                            <div className="item">
                                <img className="img-fluid" src="/img/fe/gallery/fire_extinguisher_fire_system.png" alt="fire extinguisher fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/fe/gallery/fire_extinguisher_suksindo_fire_system.png" alt="fire extinguisher suksindo fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/fe/gallery/industrial_fire_extinguisher_red_large_barrel_wheels_fire_system.png" alt="industrial fire extinguisher red large barrel wheels fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/fe/gallery/powerful_industrial_fire_extinguishing_system-fire_system.png" alt="powerful industrial fire extinguishing system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/fe/gallery/suksindo_fire_extinguisher_fire_system.png" alt="suksindo fire extinguisher fire system"/>
                            </div>
                        </OwlCarousel>
                    </Container>
                </section>
            </Fragment>
        );
    }
}
export default GalleryProduct;