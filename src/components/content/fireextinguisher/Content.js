import React, { Component, Fragment } from "react"
import { Container, Row, Col } from 'react-bootstrap'


class Content extends Component {
    render() {
        return (
            <Fragment>
                <section className="fireextinguisher-area">
                    <Container>
                        <div className="d-none d-lg-block">
                            <span className="dot" style={{ backgroundImage: 'url(/img/dots.png)' }}></span>
                            <span className="rectangle" style={{ backgroundImage: 'url(/img/Rectangle.png)' }}></span>
                            <span className="polygon2" style={{ backgroundImage: 'url(/img/Polygon2.png)' }}></span>
                        </div>
                        <Row className="align-items-center">
                            <Col xs={12} lg={6} className="wow bounceInLeft" data-wow-delay=".5s">
                                <h2 className="title">Fire Extinguisher</h2>
                                <h3>Apa itu APAR/APAB ?</h3>
                                <p>Alat perlindungan kebakaran aktif yang digunakan untuk memadamkan atau mengendalikan kebakaran kecil, seringkali dalam situasi darurat. Ini tidak dimaksudkan untuk digunakan pada api di luar kendali, seperti yang telah mencapai langit-langit, membahayakan pengguna, atau membutuhkan keahlian pemadam kebakaran.</p>
                            </Col>
                            <Col xs={12} lg={6} className="wow bounceInRight" data-wow-delay=".5s">
                                <div className="img-thumb">
                                    <img className="img-fluid" src="/img/fe/fire_extinguisher_group.jpg" alt="Fire Extinguisher Group" />
                                </div>
                            </Col>
                            <Col xs={12} lg={12} className=" my-lg-5 py-lg-5 wow fadeIn" data-wow-delay=".7s">
                                <p className="text-center detail">PT. RASA NDAY SUKSINDO melayani segala kebutuhan pelanggan akan alat pemadam api. kami menyediakan produk alat pemadam api skala kecil maupun besar, dan juga pengisian ulang dengan berbagai merk dan type yang digunakan, baik buruk kering, CO2, Foam, maupun HCFC (Liquid Gas).</p>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </Fragment>
        )
    }
}

export default Content;