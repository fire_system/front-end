import React, { Component, Fragment } from 'react'
import { Container, Row, Col } from 'react-bootstrap'

class Serve extends Component {
    render() {
        return (
            <Fragment>
                <section className="serve-area wow bounceIn" data-wow-delay=".5s">
                    <Container>
                        <Row>
                            <Col xs={12} lg={4} className="my-3 my-lg-5">
                                <div className="icon">
                                    <span>D</span>
                                </div>
                                <div className="detail">
                                    <p>Dry Chemical Powder Fire Extinguisher</p>
                                </div>
                            </Col>
                            <Col xs={12} lg={4} className="my-3 my-lg-5">
                                <div className="icon">
                                    <span>F</span>
                                </div>
                                <div className="detail">
                                    <p>Foam Fire Extinguisher</p>
                                </div>
                            </Col>
                            <Col xs={12} lg={4} className="my-3 my-lg-5">
                                <div className="icon">
                                    <span>C</span>
                                </div>
                                <div className="detail">
                                    <p>CO2 Fire Extinguisher</p>
                                </div>
                            </Col>
                            <Col xs={12} lg={4} className="my-3 my-lg-5">
                                <div className="icon">
                                    <span>F</span>
                                </div>
                                <div className="detail">
                                    <p>Fire Detect Automatic Fire Extinguisher</p>
                                </div>
                            </Col>
                            <Col xs={12} lg={4} className="my-3 my-lg-5">
                                <div className="icon">
                                    <span>T</span>
                                </div>
                                <div className="detail">
                                    <p>Thermatic Fire Extinguisher</p>
                                </div>
                            </Col>
                            <Col xs={12} lg={4} className="my-3 my-lg-5">
                                <div className="icon">
                                    <span>M</span>
                                </div>
                                <div className="detail">
                                    <p>Mobile Foam on Trolley</p>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </Fragment>
        );
    }
}

export default Serve;