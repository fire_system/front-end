import React, { Component, Fragment } from "react"
import { Container, Row, Col } from 'react-bootstrap'


class Content extends Component {
    render() {
        return (
            <Fragment>
                <section className="firesupressionsystem-area">
                    <Container>
                        <Row className="align-items-center">
                        <Col xs={12} lg={6} className="wow bounceInLeft" data-wow-delay=".5s">
                                <h2 className="title">Alarm System</h2>
                                <h3>Apa itu Alarm System ?</h3>
                                <p>Alarm system adalah sistem penanda terhadap bahaya kebakaran, bekerja untuk mendeteksi keberadaan bahaya api kebakaran yang tidak diinginkan dengan memonitor perubahan lingkungan yang terkait dengan pembakaran. Secara sederhana, cara kerja alarm system adalah dengan mengeluarkan signal seperti suara alarm dan indikasi lampu menyala apabila detektor menemukan salah satu atau beberapa tanda bahaya kebakaran seperti asap, api, gas, maupun panas.</p>
                                <p>Alarm system mendeteksi secara dini terhadap kebakaran dan akan memberitahukan berupa suara alarm kepada orang disekitar tempat kejadian agar dapat melakukan evakuasi atau tindakan darurat dalam pemadaman api.</p>
                            </Col>
                            <Col xs={12} lg={6} className="wow bounceInRight" data-wow-delay=".5s">
                                <div className="img-thumb">
                                    <img className="img-fluid" src="/img/as/Alarm_system.png" alt="Alarm system" />
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </Fragment>
        )
    }
}

export default Content;