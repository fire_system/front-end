import React, { Component, Fragment } from 'react'
import { Container } from 'react-bootstrap'
import OwlCarousel from 'react-owl-carousel'

class GalleryProduct extends Component {
    render() {
        const options = {
            margin: 30,
            loop: true,
            navElement: 'div',
            dots: true,
            nav: false,
            autoplay: true,
            smartSpeed: 2000,
            responsive: {
                0: { items: 1 },
                600: { items: 2 },
                1366: { items: 3 }
            }
        }
        return (
            <Fragment>
                <section className="galleryproduct-area wow fadeIn" data-wow-delay=".5s">
                    <Container>
                        <h2 className="">Gallery Product</h2>
                        <OwlCarousel className="my-3 my-lg-5 owl-theme align-items-center" {...options}>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/boiler_room_equipment_modern_heating_system_fire_system.png" alt="boiler room equipment modern heating system fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/ceiling_type_fire_sprinkler_building_fire_system.png" alt="ceiling type fire sprinkler building fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/fire_alarm_box_fire_system.png" alt="fire alarm box fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/fire_alarm_fire_system.png" alt="fire alarm fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/fire_alarm_gedung_fire_system.png" alt="fire alarm gedung fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/fire_alarm_wall_fire_system_suksindo.png" alt="fire alarm wall fire system suksindo"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/fire_alarm_wall_use_fire_accident_fire_system.png" alt="fire alarm wall use fire accident fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/fire_bell_flashing_warning_light_building_concepts_fire_alarm_prevention_safety_system_fire_system.png" alt="fire bell flashing warning light building concepts fire alarm prevention safety system fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/fire_detector_fire_system.png" alt="fire detector fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/fire_security_equipment_blueprint_table_good_security_service_engineering_company_fire_system.png" alt="fire security equipment blueprint table good security service engineering company fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/fire_security_equipment_good_security_service_engineering_company_fire_system.png" alt="fire security equipment good security service engineering company fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/fire_system_push_emergency_switch_door_release_fire_exit_building.png" alt="fire system push emergency switch door release fire exit building"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/fire_system_terbaik_suksindo.png" alt="fire system terbaik suksindo"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/light_fire_alarm_system_ceiling_production_room_fire_system.png" alt="light fire alarm system ceiling production room fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/manual_pull_down_station_placed_wall_fire_alarm_system_case_fire_occurred_factory_it_sequipment_safety_workplace_case_fire_emergency_fire_system.png" alt="manual pull down station placed wall fire alarm system case fire occurred factory it sequipment safety workplace case fire emergency fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/power_supply_box_fire_alarm_equipment_cctv_camera_security_servise_fire_system.png" alt="power supply box fire alarm equipment cctv camera security servise fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/smoke_alarm_fire_system_suksindo.png" alt="smoke alarm fire system suksindo"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/smoke_carbon_monoxide_alarm_fire_system.png" alt="smoke carbon monoxide alarm fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/smoke_detector_built_into_wooden_ceiling_fire_system.png" alt="smoke detector built into wooden ceiling fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/smoke_detector_ceiling_fire_system.png" alt="smoke detector ceiling fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/smoke_detector_fire_system_suksindo.png" alt="smoke detector fire system suksindo"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/smoke_detector_mounted_roof_hotel_fire_alarm_safety_equipment_house_fire_system.png" alt="smoke detector mounted roof hotel fire alarm safety equipment house fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/smoke_detector_pendent_fire_sprinkler_ceiling_fire_emergency_fire_system.png" alt="smoke detector pendent fire sprinkler ceiling fire emergency fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/smoke_detector_with_white_smoke_red_warning_light_fire_system.png" alt="smoke detector with white smoke red warning light fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/as/gallery/white_smoke_detector_ceiling_fire_system.png" alt="white smoke detector ceiling fire system"/>
                            </div>
                        </OwlCarousel>
                    </Container>
                </section>
            </Fragment>
        );
    }
}
export default GalleryProduct;