import React, { Component, Fragment } from "react"
import { Container, Row, Col } from 'react-bootstrap'


class Content extends Component {
    render() {
        return (
            <Fragment>
                <section className="hydrantequipment-area">
                    <Container>
                        <div className="d-none d-lg-block">
                            <span className="dot" style={{ backgroundImage: 'url(/img/dots.png)' }}></span>
                            <span className="rectangle" style={{ backgroundImage: 'url(/img/Rectangle.png)' }}></span>
                            <span className="polygon2" style={{ backgroundImage: 'url(/img/Polygon2.png)' }}></span>
                        </div>
                        <Row className="align-items-center">
                            <Col xs={12} lg={6} className="wow bounceInLeft" data-wow-delay=".5s">
                                <h2 className="title">Fire Protection</h2>
                                <h3>Hydrant Equipment</h3>
                                <p>Adalah sistem pemadam api yang menggunakan media, secara sistemnya tidak berbeda dengan sistem pompa air yang ada dirumah, dimana terdiri atas: Tempat penyimpanan air (Reservoir) Sistem distribusi. Hydrant Equipment sangat krusial dikarenakan supaya jika terjadi hal yang tidak diinginkan seperti kebakaran maka, calon pengguna atau pun anda bisa memanfaatkan alat proteksi kebakaran tersebut dengan baik dan benar.</p>
                            </Col>
                            <Col xs={12} lg={6} className="wow bounceInRight" data-wow-delay=".5s">
                                <div className="img-thumb">
                                    <img className="img-fluid" src="/img/he/hydrant_equipment.png" alt="Hydrant Equipment" />
                                </div>
                            </Col>
                            <Col xs={12} lg={12} className=" my-lg-5 py-lg-5 wow fadeIn" data-wow-delay=".7s">
                                <p className="text-center detail">PT. RASA NDAY SUKSINDO menyediakan berbagai macam produk peralatan Hydrant dengan kualitas terbaik. Tidak hanya menyediakan produk, kami juga menawarkan pelayanan jasa instalasi Hydrant system dengan didukung oleh team ahli yang telah memiliki pengalaman kerja. adapun produk-produk pendukung lainnya yang kami sediakan meliputi:</p>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </Fragment>
        )
    }
}

export default Content;