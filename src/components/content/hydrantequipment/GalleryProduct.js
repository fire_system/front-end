import React, { Component, Fragment } from 'react'
import { Container } from 'react-bootstrap'
import OwlCarousel from 'react-owl-carousel'

class GalleryProduct extends Component {
    render() {
        const options = {
            margin: 30,
            loop: true,
            navElement: 'div',
            dots: true,
            nav: false,
            autoplay: true,
            smartSpeed: 2000,
            responsive: {
                0: { items: 1 },
                600: { items: 2 },
                1366: { items: 3 }
            }
        }
        return (
            <Fragment>
                <section className="galleryproduct-area wow bounceInDown" data-wow-delay=".5s">
                    <Container>
                        <h2 className="">Gallery Product</h2>
                        <OwlCarousel className="my-3 my-lg-5 owl-theme" {...options}>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/big_valve_water_pipe_connection_fire_system.png" alt="big valve water pipe connection fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/fire_hose_cabinet_fire_protection_factory_fire_system.png" alt="fire hose cabinet fire protection factory fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/fire_system_terbaik_suksindo.png" alt="fire system terbaik suksindo"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/gate_valve_fire_system_suksindo.png" alt="gate valve fire system suksindo"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/gate_valve_suksindo_fire_system.png" alt="gate valve suksindo fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/hydrant_equipment_fire_system.png" alt="hydrant equipment fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/hydrant_equipment_fire_system_pvc_pipe_connection_isolated_suksindo.png" alt="hydrant equipment fire system pvc pipe connection isolated suksindo"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/industrial_tube_piplene_with_red_valves_fire_system.png" alt="industrial tube piplene with red valves fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/modern_boiler_room_equipment_high_power_burner_fire_system.png" alt="modern boiler room equipment high power burner fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/modern_boiler_room_equipment-high_power_burner_suksindo_fire_system.png" alt="modern boiler room equipment-high power burner suksindo fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/overhaul_pvc_water_pipes_city_fire_system.png" alt="overhaul pvc water pipes city fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/pipa_suksindo_fire_system.png" alt="pipa suksindo fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/pipa_untuk_fire_system_suksindo.png" alt="pipa untuk fire system suksindo"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/pipes_water_stack_warehouse_hydran_equipment_fire_system.png" alt="pipes water stack warehouse hydran equipment fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/pressure_gauge_showing_pressure_water_supply_pipe_fire_system.png" alt="pressure gauge showing pressure water supply pipe fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/pvc_ball_valve_pipe_white_hydrant_equipment_fire_system.png" alt="pvc ball valve pipe white hydrant equipment fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/pvc-pipes_drinking_water_fire_system_equipment.png" alt="pvc-pipes drinking water fire system equipment"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/selang_pronaar_merah_fire_system.png" alt="selang pronaar merah fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/selang_pronaar_putih_fire_system.png" alt="selang pronaar putih fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/steel_water_pipeline_chrome_pipes_closeup_reliable_plumbing_engineering_technology_fire_system_suksindo.png" alt="steel water pipeline chrome pipes closeup reliable plumbing engineering technology fire system suksindo"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/suksindo_gate_valve_fire_system.png" alt="suksindo gate valve fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/technician_checking_closeup_manometer_pipes_faucet_valves_heating_system_boiler_room_fire_system.png" alt="technician checking closeup manometer pipes faucet valves heating system boiler room fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/water_drain_pipes_isolated_white_hydrant_equipment_fire_system.png" alt="water drain pipes isolated white hydrant equipment fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/he/gallery/water_inlet_valve_graph_paper_fire_system.png" alt="water inlet valve graph paper fire system"/>
                            </div>
                        </OwlCarousel>
                    </Container>
                </section>
            </Fragment>
        );
    }
}
export default GalleryProduct;