import React, { Component, Fragment } from 'react'
import { Container } from 'react-bootstrap'

class Hero extends Component {
    render() {
        return(
            <Fragment>
                <section className="hero-area" style={{backgroundImage: "url(/img/banner/Hydrant_equipment-min.jpg)"}}>
                    <Container>
                        <div className="section-heading">
                            <h1 className="title wow fadeIn" data-wow-delay=".5s">Hydrant Equipment</h1>
                        </div>
                    </Container>
                </section>
            </Fragment>
        );
    }
}

export default Hero;