import React, { Component, Fragment } from 'react';
import { Container } from 'react-bootstrap';


class Newsletter extends Component {
    render() {
        return (
            <Fragment>
                <section className="newsletter_area">
                    <Container>
                        <div className="newsletter-content relative-position d-flex align-items-center justify-content-center">
                            <div className="newsletter-title text-center my-5">
                                <p className="tag-line">Percayakan pada kami Kebutuhan Alat Pemadam Kebakaran Anda</p>
                                <h2 className="title mt-4">Hubungi Kami Sekarang</h2>
                            </div>
                        </div>
                        <div className="newsletter-ellipse-1" style={{backgroundImage: 'url(/img/Ellipse-1.png)'}}></div>
                        <div className="newsletter-ellipse-2" style={{backgroundImage: 'url(/img/Ellipse-2.png)'}}></div>
                    </Container>
                </section>
            </Fragment>
        );
    }
}

export default Newsletter;