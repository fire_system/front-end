import React, { Component, Fragment } from 'react'
import { Container, Row, Col } from 'react-bootstrap';

class Detail extends Component {
    render() {
        return(
            <Fragment>
                <section className="detail-about_area">
                    <Container>
                        <Row className="align-items-center">
                            <Col xs={12} lg={6} className="order-lg-1 order-2 mt-3 mt-lg-0 wow bounceInLeft" data-wow-delay=".5s">
                                <h2 className="my-3">Tentang Kami</h2>
                                <p className="text-about">
                                    PT. RASA NDAY SUKSINDO merupakan perusahaan yang bergerak dibidang alat pemadam kebakaran, sistem instalasi kebakaran dan perlengkapan pemadam kebakaran. Produk dan jasa yang kami miliki tidak hanya berkisar pada tabung pemadam api atau alat pemadam api ringan saja, melainkan juga meliputi Fire System di antaranya yaitu Penjualan dan Instalasi Produk Fire Alarm, Fire Hydrant, Fire Suppression System dan banyak lagi.
                                </p>
                                <a href="/contact" className="btn btn-hero mt-5 active">Hubungi Kami</a>
                            </Col>
                            <Col xs={12} lg={6} className="order-lg-2 order-1 wow bounceInRight" data-wow-delay=".5s">
                                <div className="thumb-about">
                                    <div className="thumb-first">
                                        {/* <img className="img-fluid" src="/img/about/img-1.png" alt="" /> */}
                                    </div>
                                    <div className="thumb-last">
                                        {/* <img className="img-fluid" src="/img/about/img-2.png" alt="" /> */}
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
                <section className="detail-about_area">
                    <Container>
                        <Row className="align-items-center">
                            <Col xs={12} lg={5} className="mt-3 mt-lg-0 wow bounceInLeft" data-wow-delay=".5s">
                                <div className="thumb">
                                    <img className="img-fluid" src="/img/about/img-3.png" alt="about" />
                                </div>
                            </Col>
                            <Col xs={12} lg={7} className="wow bounceInRight" data-wow-delay=".5s">
                                <p className="text-about">
                                    Sebagai bentuk konsistensi dalam menciptakan produk alat pemadam yang berkualitas dan inovatif, PT. RASA NDAY SUKSINDO memproduksi Fire Extinguisher dengan merk "PRONAAR". Produk-produk kami telah teruji oleh dinas-dinas terkait sebagai produk-produk yang layak dan berkualitas tinggi. kami selalu menekankan pada aspek profesional, berorientasi pada kualitas dan ketepatan waktu dalam memenuhi kebutuhan customer.
                                </p>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </Fragment>
        );
    }
}

export default Detail;