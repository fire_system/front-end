import React,  { Fragment } from 'react'
import { Container, Row, Col } from 'react-bootstrap'

function Paralax() {
    return(
        <Fragment>
            <section className="paralax-area">
                <Container>
                    <Row className="align-items-center justify-content-center wow fadeIn" data-wow-delay=".5s">
                        <Col xs={12} lg={8} className="mx-auto">
                            <div className="content px-3 px-lg-0">
                                <blockquote><q>Kami selalu menekankan pada aspek profesional, berorientasi pada kualitas dan ketepatan waktu dalam memenuhi kebutuhan customer</q></blockquote>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </section>
        </Fragment>
    );
}

export default Paralax;