import React, { Fragment } from 'react'
import { Container, Row, Col } from 'react-bootstrap'

function VisiMisi() {
    return(
        <Fragment>
            <section className="visimisi-area mb-0">
                <Container>
                    <div className="bg-visimisi">
                        <Row className="align-items-center">
                            <Col xs={12} lg={6} className="wow bounceInLeft" data-wow-delay=".5s">
                                <div className="thumb overlay">
                                    <img className="img-fluid" src="/img/about/About_us_03.png" alt="visi & misi" />
                                </div>
                            </Col>
                            <Col xs={12} lg={6} className="mt-3 mt-lg-0 wow bounceInRight" data-wow-delay=".5s">
                                <p className="sub-title">Vision & Mision</p>
                                <h3 className="mt-2 mt-lg-4">Visi dan Misi Kami</h3>
                                <ul className="list-unstyled">
                                    <li>
                                        <div className="icon">
                                            <i className="fas fa-building"></i> 
                                        </div>
                                        <div className="text">
                                            <span>Visi</span>
                                            <p>Terbentuknya pemahaman yang baik dan kesadaran masyarakat mengenai ancaman serta potensi kebakaran dalam menjalani kehidupan.</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="icon">
                                            <i className="fas fa-building"></i> 
                                        </div>
                                        <div className="text">
                                            <span>Misi</span>
                                            <p>Membangun budaya kerja yang berkualitas dengan mengedepankan asas kejujuran dan profesionalitas serta berkomitmen untuk terus meningkatkan kualitas produk dan layanan.</p>
                                        </div>
                                    </li>
                                </ul>
                            </Col>
                        </Row>
                    </div>
                </Container>
            </section>
            <section className="mt-0">
                <div className="overlay wow fadeIn" data-wow-delay=".5s">
                    <Container>
                        <h4 className="visimisi-title">Goal Kami</h4>
                        <Row>
                            <Col lg={10}>
                                <p>Mendapatkan kepercayaan masyarakat dalam menyediakan produk dan layanan sehingga dapat menjadi sahabat dalam membangun kesadaran masyarakat mengenai ancaman kebakaran.</p>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </section>
        </Fragment>
    );
}

export default VisiMisi;