import React, { Component, Fragment } from 'react'
import { Container, Row, Col } from 'react-bootstrap'

class GalleryProduct extends Component {
    render() {
        return (
            <Fragment>
                <section className="customergallery-area wow bounceInUp" data-wow-delay=".5s">
                    <Container>
                        <h2 className="title">Our Customers</h2>
                        <Row className="align-items-center justify-content-center text-center">
                            <Col xs={12} lg={3}>
                                <div className="client-item">
                                    <img className="img-fluid" src="/img/client/clinet-bakrie.png" alt="clien bakrie" />
                                </div>
                            </Col>
                            <Col xs={12} lg={3}>
                                <div className="client-item">
                                    <img className="img-fluid" src="/img/client/clinet-mandiri.png" alt="clien mandiri" />
                                </div>
                            </Col>
                            <Col xs={12} lg={3}>
                                <div className="client-item">
                                    <img className="img-fluid" src="/img/client/clinet-aviatour.png" alt="clien aviatour" />
                                </div>
                            </Col>
                            <Col xs={12} lg={3}>
                                <div className="client-item">
                                    <img className="img-fluid" src="/img/client/clinet-rimba.png" alt="clien rimba" />
                                </div>
                            </Col>
                            <Col xs={12} lg={3}>
                                <div className="client-item">
                                    <img className="img-fluid" src="/img/client/clinet-kuri.png" alt="clien kuri" />
                                </div>
                            </Col>
                            <Col xs={12} lg={3}>
                                <div className="client-item">
                                    <img className="img-fluid" src="/img/client/clinet-ybtci.png" alt="clien ybtci" />
                                </div>
                            </Col>
                            <Col xs={12} lg={3}>
                                <div className="client-item">
                                    <img className="img-fluid" src="/img/client/clinet-mustikaratu.png" alt="clien mustikaratu" />
                                </div>
                            </Col>
                            <Col xs={12} lg={3}>
                                <div className="client-item">
                                    <img className="img-fluid" src="/img/client/clinet-ptlautanluastbk.png" alt="clien ptlautanluastbk" />
                                </div>
                            </Col>
                            <Col xs={12} lg={3}>
                                <div className="client-item">
                                    <img className="img-fluid" src="/img/client/clinet-etihad.png" alt="clien etihad" />
                                </div>
                            </Col>
                            <Col xs={12} lg={3}>
                                <div className="client-item">
                                    <img className="img-fluid" src="/img/client/clinet-ipc.png" alt="clien ipc" />
                                </div>
                            </Col>
                            <Col xs={12} lg={3}>
                                <div className="client-item">
                                    <img className="img-fluid" src="/img/client/clinet-synergy.png" alt="clien synergy" />
                                </div>
                            </Col>
                            <Col xs={12} lg={3}>
                                <div className="client-item">
                                    <img className="img-fluid" src="/img/client/clinet-hestiwirasakti.png" alt="clien hestiwirasakti" />
                                </div>
                            </Col>
                            <Col xs={12} lg={3}>
                                <div className="client-item">
                                    <img className="img-fluid" src="/img/client/clinet-telkom.png" alt="clien telkom" />
                                </div>
                            </Col>
                            <Col xs={12} lg={3}>
                                <div className="client-item">
                                    <img className="img-fluid" src="/img/client/clinet-mnctv.png" alt="clien mnctv" />
                                </div>
                            </Col>
                            <Col xs={12} lg={3}>
                                <div className="client-item">
                                    <img className="img-fluid" src="/img/client/clinet-agrinesia.png" alt="clien agrinesia" />
                                </div>
                            </Col>
                            <Col xs={12} lg={3}>
                                <div className="client-item">
                                    <img className="img-fluid" src="/img/client/clinet-jkmp.png" alt="clien jkmp" />
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </Fragment>
        );
    }
}
export default GalleryProduct;