import React, { Component, Fragment } from "react"
import { Container, Row, Col } from 'react-bootstrap'


class Content extends Component {
    render() {
        return (
            <Fragment>
                <section className="firesupressionsystem-area wow fadeIn" data-wow-delay=".5s">
                    <Container>
                        <div className="d-none d-lg-block">
                            <span className="rectangle" style={{ backgroundImage: 'url(/img/Rectangle.png)' }}></span>
                        </div>
                        <Row className="align-items-center">
                            <Col xs={12} className="text-center">
                                <h3>Riwayat Perusahaan</h3>
                                <p>Produk-produk dari PT. RASA NDAY SUKSINDO telah digunakan oleh berbagai macam lembaga di Indonesia, baik korporasi, lembaga kesehatan hingga kementerian. Kami PT. RASA NDAY SUKSINDO sangat menekankan komitmen untuk selalu menjaga kepuasan dan kenyamanan customer dan bertransaksi kami memiliki team yang kooperatif serta profesional dalam melayani kebutuhan customer. Sesuai dengan misi lembaga kami yang akan menjadi sahabat dalam mencegah terjadinya kebakaran.</p>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </Fragment>
        )
    }
}

export default Content;