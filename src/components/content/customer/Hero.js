import React, { Component, Fragment } from 'react'
import { Container } from 'react-bootstrap'

class Hero extends Component {
    render() {
        return(
            <Fragment>
                <section className="hero-area" style={{backgroundImage: "url(/img/banner/our_customer_fire_system-min.jpg)"}}>
                    <Container>
                        <div className="section-heading">
                            <h1 className="title wow fadeIn" data-wow-delay=".5s">Our Customers</h1>
                        </div>
                    </Container>
                </section>
            </Fragment>
        );
    }
}

export default Hero;