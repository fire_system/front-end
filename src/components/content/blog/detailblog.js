import React from 'react'
import Interweave from 'interweave'
import moment from 'moment'
import dateFormat from 'dateformat'

import { LineShareButton,FacebookShareButton,TwitterShareButton,WhatsappShareButton,LineIcon,FacebookIcon,TwitterIcon,WhatsappIcon } from 'react-share'

const DetailBlog = (props) => <section className="detail_news-area mx-1">
<img className="img-fluid" src={"https://dashboard.firesystem.co.id/storage/thumbnail/" + props.thumbnail} alt={props.title} />
<div className="section--body">
    <ul className="list-unstyled d-flex blog-meta">
        <li><i className="fas fa-user"></i> {props.author}</li>
        <li><i className="fas fa-clock"></i> {moment(dateFormat(props.updated_at, "yyyy,mm,dd")).fromNow()}</li>
    </ul>
    <Interweave content={props.content} />
    <strong>Bagikan : </strong>
    <div className="meta-sosmed">
        <FacebookShareButton title={props.title} url={'https://firesystem.co.id/blog/'+ props.category_slug +'/'+ props.slug }>
            <FacebookIcon size={40} round={true} />
        </FacebookShareButton>
        <TwitterShareButton title={props.title} url={'https://firesystem.co.id/blog/'+ props.category_slug +'/'+ props.slug }>
            <TwitterIcon size={40} round={true} />
        </TwitterShareButton>
        <WhatsappShareButton title={props.title} url={'https://firesystem.co.id/blog/'+ props.category_slug +'/'+ props.slug }>
            <WhatsappIcon size={40} round={true} />
        </WhatsappShareButton>
        <LineShareButton title={props.title} url={'https://firesystem.co.id/blog/'+ props.category_slug +'/'+ props.slug }>
            <LineIcon size={40} round={true} />
        </LineShareButton>
    </div>
    <ul className="meta-tags d-lg-flex">
    {
        props.tags.split(',').map((tag, i) => {
        return(
            <li key={i}>{ tag }</li>
            );
        })
    }
    </ul>
</div>
</section>


export default DetailBlog;