import React, { Component, Fragment } from 'react'

class Tags extends Component {
    render() {
        return (
            <Fragment>
                <section className="tags-area">
                    <div className="section-heading">
                        <h3>Tags</h3>
                    </div>
                    <div className="section-body">
                        <ul className="list-ustyled d-flex align-items-center">
                            <li><a href="/">sample</a></li>
                            <li><a href="/">sample</a></li>
                        </ul>
                    </div>
                </section>
            </Fragment>
        );
    }
}

export default Tags;