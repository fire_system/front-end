import React, { Component, Fragment } from 'react'
import { Spinner } from 'react-bootstrap'
import ListBlog from './listblog'

class Content extends Component {
    state = {
        news: [],
        per: 4,
        page: 1,
        totalPages: null,
        scrolling: false,
    }

    componentDidMount() {
        this.loadListBlog()
        this.scrollListener = window.addEventListener('scroll', (e) => {
            this.handleScrollBlog(e)
        })
    }

    handleScrollBlog = (e) => {
        const { scrolling, totalPages, page } = this.state
        if (scrolling) return
        if (totalPages <= page) return
        const lastLi = document.querySelector('ul.list-blog-array > li:last-child')
        const lastLiOffset = lastLi.offsetTop + lastLi.clientHeight
        const pageOffset = window.pageYOffset + window.innerHeight
        var bottomOffset = 4
        if (pageOffset > lastLiOffset - bottomOffset) this.loadMore()
    }
    
    loadListBlog = () => {
        const { per, page, news } = this.state;
        const url = process.env.REACT_APP_API_URL + `/newsapi?per=${per}&page=${page}`
        // console.log(url);
        fetch(url, { headers: { Authorization: 'Bearer '.concat(process.env.REACT_APP_TOKEN) } })
        .then(response => response.json())
        .then(json => this.setState({
            news : [...news, ...json.data.sources],
            scrolling: false,
            totalPages: json.data.paginate.total_pages,
        }))
    }

    loadMore = () => {
        this.setState(prevState => ({
            page: prevState.page + 1,
            scrolling: true,
        }), this.loadListBlog)
    }
    
    render() {
        return(
            <Fragment>
                <section className="contentblog-area">
                    <ul className="list-blog-array list-unstyled">
                        {
                            this.state.news.length > 0 ? this.state.news.map((blog, i) => {
                                return(
                                    <li key={i}>
                                        <ListBlog  {...blog}/>
                                    </li>
                                );
                            }) : 
                            <div className="d-flex align-items-center justify-content-center text-center">
                                <Spinner animation="border" variant="danger" role="status"><span className="sr-only">Loading...</span></Spinner>
                            </div>
                        }
                    </ul>
                </section>
            </Fragment>
        );
    }
}

export default Content;