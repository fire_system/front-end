import React, { Component, Fragment } from 'react'
import { Form } from 'react-bootstrap'
import { ReactSearchAutocomplete } from "react-search-autocomplete"

class Search extends Component {
    state = {
        listSearch: [],
    }

    componentDidMount() {
        this.loadListBlogSearch()
    }

    loadListBlogSearch = (e) => {
        const { listSearch } = this.state
        const url = process.env.REACT_APP_API_URL + `/newsapi`
        fetch(url, { headers: { Authorization: 'Bearer '.concat(process.env.REACT_APP_TOKEN) } })
        .then(response => response.json())
        .then(json => this.setState({
            listSearch : [...listSearch, ...json.data.sources],
            scrolling: false
        }))
    }

    render() {
        const serachBlog = this.state.listSearch;
        // console.log(serachBlog);
        // console.log(this)

        const handleOnSearch = (string, results) => {
            // console.log(string, results);
        };
        
        const handleOnSelect = (item) => {
            return window.open('/blog/'+item.category.slug+'/'+item.slug);
        };
        
        const handleOnFocus = () => {
            // console.log("Focused");
        };
        
        return (
            <Fragment>
                <section className="search-area">
                    <Form>
                        {/* <InputGroup>
                            <Form.Control type="search" placeholder="Search..." aria-label="Username" aria-describedby="basic-addon1"/>
                            <InputGroup.Prepend>
                                <Button variant="outline-secondary"><i className="fas fa-search"></i></Button>
                            </InputGroup.Prepend>
                        </InputGroup> */}
                        <ReactSearchAutocomplete
                            items={serachBlog}
                            fuseOptions={{ keys: ['title'] }}
                            resultStringKeyName="title"
                            onSearch={handleOnSearch}
                            onSelect={handleOnSelect}
                            onFocus={handleOnFocus}
                            styling={{ zIndex: 1 }} // To display it on top of the search box below
                            autoFocus
                        />
                    </Form>
                </section>
            </Fragment>
        );
    }
}

export default Search;