import React, { Component, Fragment } from 'react'

class Category extends Component {
    state = {
        arrCategory: [],
    }
    componentDidMount() {
        this.loadarrCategory()
    }
    loadarrCategory = () => {
        const {  arrCategory } = this.state;
        const url = process.env.REACT_APP_API_URL + `/categoryapi`
        // console.log(url);
        fetch(url, { headers: { Authorization: 'Bearer '.concat(process.env.REACT_APP_TOKEN) } })
        .then(response => response.json())
        .then(json => this.setState({
            arrCategory : [...arrCategory, ...json.data.source],
        }))
    }
    render() {
        return (
            <Fragment>
                <section className="category-area">
                    <div className="section-heading">
                        <h3>Categories</h3>
                    </div>
                    <div className="section-body">
                        <ul className="list-unstyled">
                            <li className="">
                                <a href="/blog" className="category_name">
                                    <i className="fas fa-angle-right"></i>
                                    <span>All</span>
                                </a>
                            </li>
                        {
                            this.state.arrCategory.map((categori, i) => {
                                return(
                                    <li className="" key={i}>
                                        <a href={'/blog/'+ categori.slug} className="category_name">
                                            <i className="fas fa-angle-right"></i>
                                            <span>{ categori.name }</span>
                                        </a>
                                    </li>
                                );
                            })
                        }
                        </ul>
                    </div>
                </section>
            </Fragment>
        );
    }
}

export default Category;