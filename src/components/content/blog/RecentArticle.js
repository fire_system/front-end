import React, { Component, Fragment } from 'react'
import Interweave from 'interweave'
import moment from 'moment'
import dateFormat from 'dateformat'

class RecentArticle extends Component {
    state = {
        arrBlog: [],
    }
    componentDidMount() {
        this.loadarrBlog()
    }
    loadarrBlog = () => {
        const {  arrBlog } = this.state;
        const url = process.env.REACT_APP_API_URL + `/newsapi`
        // console.log(url);
        fetch(url, { headers: { Authorization: 'Bearer '.concat(process.env.REACT_APP_TOKEN) } })
        .then(response => response.json())
        .then(json => this.setState({
            arrBlog : [...arrBlog, ...json.data.sources],
        }))
    }
    render() {
        return (
            <Fragment>
                <section className="recentarticle-area">
                    <div className="section-heading">
                        <h3>Recent Articles</h3>
                    </div>
                    <div className="section-body">
                    {
                        this.state.arrBlog.slice(0, 3).map((blog, i) => {
                            return(
                            <div className="single-blog d-flex align-items-center mb-2" key={i}>
                                <a href={'/blog/'+ blog.category.slug + '/' + blog.slug} title={blog.title} className="img-thumb">
                                    {/* <img className="img-fluid" src="/img/blog/blog-detail.png" alt=""/> */}
                                    <div className="thumb-img" style={{ backgroundImage: 'url(https://dashboard.firesystem.co.id/storage/thumbnail/'+  blog.thumbnail +')' }}></div>
                                </a>
                                <div className="text">
                                    <h4 className="heading">
                                        <a href={'/blog/'+ blog.category.slug + '/' + blog.slug} title={blog.title}>
                                            <Interweave content={blog.title}/>
                                        </a>
                                    </h4>
                                    <div className="meta">
                                        <div>
                                            {/* <span className="icon-calendar"></span> {moment([blog.updated_at]).fromNow()} */}
                                            <span className="icon-calendar"></span> {moment(dateFormat(blog.updated_at, "yyyy,mm,dd")).fromNow()}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            );
                        })
                    }
                    </div>
                </section>
            </Fragment>
        );
    }
}

export default RecentArticle;