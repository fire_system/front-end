import React from 'react'
import Interweave from 'interweave'
import moment from 'moment'
import dateFormat from 'dateformat'

const ListBlog = (props) => <div className="single-blog-post d-lg-flex align-items-center mb-3" key={props.id}>
<a href={'/blog/'+ props.category.slug + '/' + props.slug} className="blog-thumb" title={props.title}>
    <div className="thumb-img" style={{ backgroundImage: 'url(https://dashboard.firesystem.co.id/storage/thumbnail/'+ props.thumbnail +')' }}></div>
</a>
<div className="blog-content">
    <h2 className="blog-title">
        <a href={'/blog/'+ props.category.slug + '/' + props.slug} title={props.title}><Interweave content={props.title} /></a>
    </h2>
    <ul className="list-unstyled d-flex blog-meta">
        <li><i className="fas fa-user"></i> {props.author}</li>
        <li><i className="fas fa-clock"></i> {moment(dateFormat(props.updated_at, "yyyy,mm,dd")).fromNow()}</li>
    </ul>
    <div className="blog-desc"><Interweave content={props.content}/></div>
    <a href={'/blog/'+ props.category.slug + '/' + props.slug} className="btn btn-readmore" title={props.title}>Read More</a>
</div>
</div>

export default ListBlog;