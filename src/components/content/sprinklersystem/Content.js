import React, { Component, Fragment } from "react"
import { Container, Row, Col } from 'react-bootstrap'
import OwlCarousel from 'react-owl-carousel'
class Content extends Component {
    render() {
        const options = {
            margin: 30,
            loop: true,
            navElement: 'div',
            dots: true,
            nav: false,
            autoplay: true,
            smartSpeed: 2000,
            responsive: {
                0: { items: 1 },
                600: { items: 2 },
                1366: { items: 3 }
            }
        }
        return (
            <Fragment>
                <section className="spinklersystemimg-area wow fadeIn" data-wow-delay=".8s">
                    <Container>
                        <div className="d-none d-lg-block">
                            <span className="polygon" style={{ backgroundImage: 'url(/img/Polygon.png)' }}></span>
                            <span className="ellipse" style={{ backgroundImage: 'url(/img/Ellipse.png)' }}></span>
                        </div>
                        <Row className="align-items-center">
                            <Col xs={12} lg={6}>
                                <h2 className="title">Sprinkler System</h2>
                                <h3>Apa Itu Sprinkler System?</h3>
                                <p>Sprinkler System adalah alat yang berguna untuk memadamkan api secara otomatis, Pada umumnya sprinkler di pasang pada gedung yang memiliki ketinggan Diatas Lima Lantai atau tempat yang memiliki fasilitas umum dan memiliki tingkat bahaya kebakaran seperti apartemen,rumah sakit,pabrik, kantor dan gedung - gedung lainnya.</p>
                            </Col>
                            <Col xs={12} lg={6}>
                                <div className="img-thumb">
                                    <img className="img-fluid" src="/img/ss/sprinkler_alarm.png" alt="Sprinkler Alarm" />
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
                <section className="spinklersystemslide-area wow bounceInUp" data-wow-delay="1s">
                    <Container>
                        <div className="d-none d-lg-block">
                            <span className="dots" style={{ backgroundImage: 'url(/img/dots.png)' }}></span>
                            <span className="rectangle" style={{ backgroundImage: 'url(/img/Rectangle.png)' }}></span>
                            <span className="ellipse" style={{ backgroundImage: 'url(/img/Ellipse.png)' }}></span>
                            <span className="circle2" style={{ backgroundImage: 'url(/img/double-circle-2.png)' }}></span>
                        </div>
                        <h2 className="">Gallery Product</h2>
                        <OwlCarousel className="my-3 my-lg-5 owl-theme" {...options}>
                            <div className="item">
                                <img className="img-fluid" src="/img/ss/gallery/air_duct_air_conditioner_pipe_wiring_pipe_fire_sprinkler_system_air_flow_ventilation_system_building_interior_ceiling_lamp_light_with_opened_light_sprinkler_alarm_fire_system.png" alt="air duct air conditioner pipe wiring pipe fire sprinkler system air flow ventilation system building interior ceiling lamp light with opened light sprinkler alarm fire system" />
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ss/gallery/air_duct_automatic_fire_sprinkler_safety_system_fire_protection_detector_fire-sprinkler_system_building_interior_concept_ceiling_lamp_light_with_opened_light_fire_system.png" alt="air duct automatic fire sprinkler safety system fire protection detector fire-sprinkler system building interior concept ceiling lamp light with opened light fire system" />
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ss/gallery/automatic_fire_sprinkler_safety_system_black-water_cooling_supply_pipe_fire-suppression_fire_protection_detector_fire_sprinkler_system_with_red_pipes_suksindo_fire_system.png" alt="automatic fire sprinkler safety system black-water cooling supply pipe fire-suppression fire protection detector fire sprinkler system with red pipes suksindo fire system" />
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ss/gallery/ceiling_mounted_automatic_head_fire_extinguisher_system_safety_fighting_equipment_sprinkler_ceiling_fire_system_suksindo.png" alt="ceiling mounted automatic head fire extinguisher system safety fighting equipment sprinkler ceiling fire system suksindo" />
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ss/gallery/fire_fighter_safety_fighting_equipment_sprinkler_ceiling_automatic_head_extinguisher_selected_focus_sprinkle_fire_system_suksindo.png" alt="fire fighter safety fighting equipment sprinkler ceiling automatic head extinguisher selected focus sprinkle fire system suksindo" />
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ss/gallery/heating_engineer_fixing_modern_heating_system_boiler_room_automatic_control_unit_fire_system.png" alt="heating engineer fixing modern heating system boiler room automatic control unit fire system" />
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ss/gallery/industrial_air_cooling_system_ventilation_pipes_fire_system.png" alt="industrial air cooling system ventilation pipes fire system" />
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ss/gallery/supply_exhaust_ventilation_system_ceiling_commercial_room_warehouse_fire_system.png" alt="supply exhaust ventilation system ceiling commercial room warehouse fire system" />
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ss/gallery/ventilation_system_ceiling_air-duct_large_shopping_mall_fire_system.png" alt="ventilation system ceiling air-duct large shopping mall fire system" />
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ss/gallery/water_sprinkler_and_fire_alarm_system_water_sprinkler_control_system_and_pipelines_of_industrial_fire_system.png" alt="water sprinkler and fire alarm system water sprinkler control system and pipelines of industrial fire system" />
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/ss/gallery/water_sprinkler_fire_alarm_system_water_sprinkler_control_system_fire_system.png" alt="water sprinkler fire alarm system water sprinkler control system fire system" />
                            </div>
                        </OwlCarousel>
                    </Container>
                </section>
            </Fragment>
        )
    }
}

export default Content;