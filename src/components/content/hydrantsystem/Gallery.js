import React, { Component, Fragment } from 'react'
import { Container } from 'react-bootstrap'
import OwlCarousel from 'react-owl-carousel'

class Gallery extends Component {
    render() {
        const options = {
            margin: 30,
            loop: true,
            navElement: 'div',
            dots: true,
            nav: false,
            autoplay: true,
            smartSpeed: 2000,
            responsive: {
                0: { items: 1 },
                600: { items: 2 },
                1366: { items: 3 }
            }
        }
        return (
            <Fragment>
                <section className="gallery-area wow fadeIn" data-wow-delay=".5s">
                    <Container>
                        <OwlCarousel className="my-3 my-lg-5 owl-theme" {...options}>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/construction_work_water_pipe_system_system_gray_sanitary_pipes_building_fire_system_suksindo.png" alt="construction work water pipe system system gray sanitary pipes building fire system suksindo"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/diesel_generator_for_fire_pump_at_industry_zone_fire_system.png" alt="diesel generator for fire pump at industry zone fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/fire_extinguisher_room_fire_system_suksindo.png" alt="fire extinguisher room fire system suksindo"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/fire_water_piping_system_against_wall_fire_system.png" alt="fire water piping system against wall fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/gas_pipeline_valve_boiler_back_room_communication_metal_tools_box_fire_system.png" alt="gas pipeline valve boiler back room communication metal tools box fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/heating_installation_suksindo_fire_system.png" alt="heating installation suksindo fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/hydrant_system_fire_system.png" alt="hydrant system fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/industrial_building_fire-extinguishing_system_suksindo_fire_system(1).png" alt="industrial building fire-extinguishing system suksindo fire system(1)"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/industrial_building_fire-extinguishing_system_suksindo_fire_system.png" alt="industrial building fire-extinguishing system suksindo fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/industrial_fire_pump_station_water_sprinkler_piping_fire_alarm_control_system_fire_system_suksindo.png" alt="industrial fire pump station water sprinkler piping fire alarm control system fire system suksindo"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/industrial_fire_pump_station_water_sprinkler_piping_fire_alarm_control_system_hydrant_system_fire_system.png" alt="industrial fire pump station water sprinkler piping fire alarm control system hydrant system fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/industrial_fire_pump_station_water_sprinkler_piping_fire_alarm_control_system_suksindo_fire_system.png" alt="industrial fire pump station water sprinkler piping fire alarm control system suksindo fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/modern_interior_brewery_barrels_pipes_factory_interior_fire_system.png" alt="modern interior brewery barrels pipes factory interior fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/modern_interior_brewery_barrels_pipes_factory_interior_hydrant_system_suksindo.png" alt="modern interior brewery barrels pipes factory interior hydrant system suksindo"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/panel_control_fire_system.png" alt="panel control fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/Panel_Hydrant_System_suksindo.png" alt="Panel Hydrant System suksindo"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/pipes_boiler_room_fire_system.png" alt="pipes boiler room fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/pomp_room_suksindo_fire_system.png" alt="pomp room suksindo fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/pompa_hydrant_fire_system.png" alt="pompa hydrant fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/pressure_tank_hydrant_system_suksindo_fire_system.png" alt="pressure tank hydrant system suksindo fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/pump_steel_pipelines_cooling_tower_power_plant_fire_system.png" alt="pump steel pipelines cooling tower power plant fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/pump_steel_pipelines_cooling_tower_power_plant_hydrant_equipment_fire_system.png" alt="pump steel pipelines cooling tower power plant hydrant equipment fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/red_water_pipe_industrial_building_fire_extinguishing_system_fire_system.png" alt="red water pipe industrial building fire extinguishing system fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/suksindo_fire_pipe_big_valve_water_set_line_up_long_row_fire_system.png" alt="suksindo fire pipe big valve water set line up long row fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/suksindo_manometer_pipes_faucet_valves_heating_system_boiler_room_fire_system.png" alt="suksindo manometer pipes faucet valves heating system boiler room fire system"/>
                            </div>
                            <div className="item">
                                <img className="img-fluid" src="/img/hs/gallery/water_sprinkler_fire-alarm_system_water_sprinkler_control_system_pipelines_industrial_fire_system.png" alt="water sprinkler fire-alarm system water sprinkler control system pipelines industrial fire system"/>
                            </div>
                        </OwlCarousel>
                    </Container>
                </section>
            </Fragment>
        );
    }
}
export default Gallery;