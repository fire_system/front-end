import React, { Component, Fragment } from "react"
import { Container, Row, Col } from 'react-bootstrap'


class Content extends Component {
    render() {
        return (
            <Fragment>
                <section className="hs-area wow bounceInRight" data-wow-delay="1s">
                    <Container>
                        <div className="d-none d-lg-block">
                            <span className="rectangle" style={{ backgroundImage: 'url(/img/Rectangle.png)' }}></span>
                        </div>
                        <Row className="align-items-center">
                            <Col xs={12} lg={6}>
                                <div className="img-thumb">
                                    <img className="img-fluid" src="/img/hs/apa_itu_hydrant_system.png" alt="Apa Itu Hydrant System" />
                                </div>
                            </Col>
                            <Col xs={12} lg={6}>
                                <h2 className="title">Hydrant System</h2>
                                <h3>Apa itu Hydrant System ?</h3>
                                <p>Fire hydrant adalah suatu sistem instalasi atau jaringan pemipaan berisi air bertekanan besar yang digunakan sebagai sarana untuk memadamkan kebakaran. Pada umumnya, hydrant terinstalasi di gedung-gedung, mall, dan bangunan lainnya. Fire hydrant menggunakan media air bertekanan besar untuk memadamkan api. Sistem fire hydrant ini merupakan proteksi untuk kebakaran skala besar. Sistem ini dapat beroperasi dengan dukungan komponen utama dan aksesoris pendukung. Mesin pada hydrant dapat diatur pada panel untuk bekerja secara otomatis dan manual. Meskipun demikian, sangat dibutuhkan user yang sudah terlatih untuk menjangkau titik api saat kebakaran.</p>
                            </Col>
                        </Row>
                    </Container>
                </section>
                <section className="reservoir-area wow bounceInLeft" data-wow-delay="1s">
                    <Container>
                        <div className="d-none d-lg-block">
                            <span className="dots" style={{ backgroundImage: 'url(/img/dots.png)' }}></span>
                            <span className="ellipse" style={{ backgroundImage: 'url(/img/Ellipse.png)' }}></span>
                        </div>
                        <Row className="align-items-center">
                            <Col xs={12} lg={6}>
                                <h2 className="title">Komponen di dalam Hydrant</h2>
                                <h3>Reservoir</h3>
                                <p>Reservoir bisa berada di bawah tanah (ground tank fire hydrant) atau di atas tanah (water tank) yang dapat Anda sesuaikan dengan ketersediaan tempat dan instalasi. Reservoir harus mampu mengatasi persediaan air minimal 30 menit.</p>
                            </Col>
                            <Col xs={12} lg={6}>
                                <div className="img-thumb">
                                    <img className="img-fluid" src="/img/hs/reservoir.png" alt="Reservoir" />
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
                <section className="pomproom-area wow bounceInRight" data-wow-delay="1s">
                    <Container>
                        <div className="d-none d-lg-block">
                            <span className="polygon" style={{ backgroundImage: 'url(/img/Polygon.png)' }}></span>
                        </div>
                        <Row className="align-items-center">
                            <Col xs={12} lg={6}>
                                <div className="img-thumb">
                                    <img className="img-fluid" src="/img/hs/pump_room.png" alt="Pump Room" />
                                </div>
                            </Col>
                            <Col xs={12} lg={6}>
                                <h3>Pomp Room</h3>
                                <p>Rumah pompa adalah tempat dimana komponen hydrant berada. Seperti hydrant pump, panel control, header, suction, pressure tank, dan aksesoris hydrant lainnya.</p>
                            </Col>
                        </Row>
                    </Container>
                </section>
                <section className="sistempipa-area wow bounceInLeft" data-wow-delay="1s">
                    <Container>
                        <div className="d-none d-lg-block">
                            <span className="circle1" style={{ backgroundImage: 'url(/img/double-circle-1.png)' }}></span>
                        </div>
                        <Row className="align-items-center">
                            <Col xs={12} lg={6}>
                                <h3>Sistem pipa hydrant</h3>
                                <p>Sistem pipa hydrant terdiri dari beberapa komponen pipa dengan ukuran diameter yang berbeda. Pipa ini sebagai media distribusi air untuk memadamkan kebakaran.</p>
                            </Col>
                            <Col xs={12} lg={6}>
                                <div className="img-thumb">
                                    <img className="img-fluid" src="/img/hs/system_pipa_hydrant.png" alt="System Pipa Hydrant" />
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
                <section className="pompahydrat-area wow bounceInRight" data-wow-delay="1s">
                    <Container>
                        <div className="d-none d-lg-block">
                            <span className="rectangle" style={{ backgroundImage: 'url(/img/Rectangle.png)' }}></span>
                            <span className="dots" style={{ backgroundImage: 'url(/img/dots.png)' }}></span>
                        </div>
                        <Row className="align-items-center">
                            <Col xs={12} lg={6}>
                                <div className="img-thumb">
                                    <img className="img-fluid" src="/img/hs/pompa_hydrant.png" alt="Pompa Hydrant" />
                                </div>
                            </Col>
                            <Col xs={12} lg={6}>
                                <h3>Pompa Hydrant</h3>
                                <p>Pompa hydrant berfungsi memindahkan air dari reservoir ke sistem distribusi hydrant. Terdapat tiga pompa dalam hydrant, yaitu electric main pump, diesel pump, dan jockey pump.</p>
                            </Col>
                        </Row>
                    </Container>
                </section>
                <section className="panelkontrol-area wow bounceInLeft" data-wow-delay="1s">
                    <Container>
                        <div className="d-none d-lg-block">
                            <span className="ellipse" style={{ backgroundImage: 'url(/img/Ellipse.png)' }}></span>
                            <span className="polygon2" style={{ backgroundImage: 'url(/img/Polygon2.png)' }}></span>
                        </div>
                        <Row className="align-items-center">
                            <Col xs={12} lg={6}>
                                <h3>Panel Control</h3>
                                <p>Panel control berfungsi mengatur dan mengendalikan system kerja pompa hydrant agar dapat bekerja sesuai fungsinya berdasarkan tekanan yang ada pada instalasi pipa.</p>
                            </Col>
                            <Col xs={12} lg={6}>
                                <div className="img-thumb">
                                    <img className="img-fluid" src="/img/hs/panel_control.png" alt="Panel Control" />
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
                <section className="pressuretank-area wow bounceInRight" data-wow-delay="1s">
                    <Container>
                        <div className="d-none d-lg-block">
                            <span className="circle2" style={{ backgroundImage: 'url(/img/double-circle-2.png)' }}></span>
                        </div>
                        <Row className="align-items-center">
                            <Col xs={12} lg={6}>
                                <div className="img-thumb">
                                    <img className="img-fluid" src="/img/hs/pressure_tank.png" alt="Pressure Tank" />
                                </div>
                            </Col>
                            <Col xs={12} lg={6}>
                                <h3>Pressure tank</h3>
                                <p>Fungsi dari pressure tank yaitu mejaga kestabilan tekanan dari pompa hydrant dan berfungsi untuk membuang udara yang terjebak dalam instalasi pompa hydrant.</p>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </Fragment>
        )
    }
}

export default Content;