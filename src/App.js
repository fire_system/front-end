import React, { Component, Fragment } from 'react'
import { Switch, Route } from 'react-router-dom'
import Header from './components/common/Header';
import Footer from './components/common/Footer';

import Home from './components/pages/Home'

import HydrantSystem from './components/pages/HydrantSystem'
import SprinklerSystem from './components/pages/SprinklerSystem'

import FireExtinguisherAparApab from './components/pages/FireExtinguisherAparApab'
import PersonalProtectiveEquipment from './components/pages/PersonalProtectiveEquipment'
import HydrantEquipment from './components/pages/HydrantEquipment'

import FireSupressionSystem from './components/pages/FireSupressionSystem'
import AlarmSystem from './components/pages/AlarmSystem'

import OurCustomer from './components/pages/OurCustomer'
import Dokumentasi from './components/pages/Dokumentasi'
import About from './components/pages/About'
import Contact from './components/pages/Contact'

import Blog from './components/pages/Blog'
import BlogCategori from './components/pages/BlogCategori'
import BlogDetail from './components/pages/BlogDetail'

import PrivacyPolicy from './components/pages/PrivacyPolicy'
import TermCondition from './components/pages/TermCondition'

import ScrollTop from './ScrollTop'
require('dotenv').config()

class App extends Component {
  render() {
    return (
      <Fragment>
        <ScrollTop>
          <Header/>
          <Switch>
            <Route exact path="/" component={Home} />
            
            <Route exact path="/hydrant-system" component={HydrantSystem}/>
            <Route exact path="/sprinkler-system" component={SprinklerSystem}/>
            
            <Route exact path="/fire-extinguisher-apar-apab" component={FireExtinguisherAparApab}/>
            <Route exact path="/personal-protective-equipment" component={PersonalProtectiveEquipment}/>
            <Route exact path="/hydrant-equipment" component={HydrantEquipment} />

            <Route exact path="/fire-supression-system" component={FireSupressionSystem}/>
            <Route exact path="/alarm-system" component={AlarmSystem}/>

            <Route exact path="/ourcustomers" component={OurCustomer}/>
            <Route exact path="/dokumentasi" component={Dokumentasi} />
            <Route exact path="/about" component={About}/>
            <Route exact path="/blog" component={Blog}/>
            <Route exact path="/blog/:categori" component={BlogCategori} />
            <Route path="/blog/:categori/:slug" component={BlogDetail} />
            <Route exact path="/contact" component={Contact}/>

            <Route exact path="/privacypolicy" component={PrivacyPolicy}/>
            <Route exact path="/termcondition" component={TermCondition}/>
          </Switch>
          <Footer/>
        </ScrollTop>
      </Fragment>
    );
  }
}

export default App;
